<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsignacionDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignacion_descuentos', function (Blueprint $table) {
            $table->unsignedInteger('id',1);
            $table->integer('id_empleados');
            $table->integer('id_descuentos');
            $table->decimal('valor',8,2);
            $table->unsignedInteger('forma_descuento');
            $table->integer('aplicabilidad');
            $table->date('fecha_asignacion');
            $table->decimal('cuota_mes',8,2)->nullable();
            $table->unsignedInteger('mes_inicial')->nullable();
            $table->unsignedInteger('mes_final')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignacion_descuentos');
    }
}
