<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('cantidad');
            $table->string('codigo_producto',255);
            $table->longText('descripcion');
            $table->date('fecha_de_compra');
            $table->date('fecha_ingreso');
            $table->integer('id_usuario');
            $table->integer('id_estante');
            $table->integer('id_tipo_presentacion');
            $table->string('nombre_producto',255);
            $table->string('numero_factura',255);
            $table->decimal('precio', 8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repuestos');
    }
}
