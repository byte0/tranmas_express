<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->unsignedInteger('id',1);
            $table->text('dui');
            $table->string('nombres', 50);
            $table->string('apellidos', 50);
            $table->string('nit', 25)->nullable();
            $table->string('afp', 25)->nullable();
            $table->integer('estado_civil');
            $table->string('isss',  25)->nullable();
            $table->date('fecha_nacimiento');
            $table->text('telefono');
            $table->year('ingreso');
            $table->string('direccion',200);
            $table->unsignedInteger('estado')->default(0);
            $table->unsignedInteger('id_cargo')->nullable();
            $table->unsignedInteger('id_unidad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
