<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalidaRepuestos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salida_repuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idrespuesto');
            $table->integer('idusuario_entrega');
            $table->integer('idusuario_recibe');
            $table->integer('cantidad_entregada');
            $table->date('fecha_salida');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salida_repuestos');
    }
}
