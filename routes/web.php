<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

/*
|------------------------------------------------------------------------------------------|
|   Rutas para obtener, crear, modificar y eliminar (CRUD) de los controladores            |
|    Restfull                                                                       |
|------------------------------------------------------------------------------------------|
*/
Route::resources
    ([
        'cargos' => 'CargosController',
        'unidades' => 'UnidadesController',
        'planillas' => 'PlanillasController',
        'descuento' => 'DescuentoController',
        'empleados' => 'EmpleadosController',
        'productos' => 'ProductosController',
        'repuestos' => 'RepuestosController',
        'salida-repuestos' => 'Salida_RepuestosController',
        'presentacion' => 'Tipo_PresentacionController',
        'asignacion-descuentos' => 'Asignacion_DescuentosController'
    ]);

//Ruta home donde se le da la vienvenida la usuario logueado
Route::get('/home', 'HomeController@index')->name('home');

/*
 |------------------------------------
 |          EmpleadosController       |
 |------------------------------------
 */

// Rta buscar para el controlador EmpleadosController
Route::post('/empleados/search', 'EmpleadosController@search');

// Ruta para descargar el pdf para el controlador  EmpleadosController
Route::get('/empleados/pdf/{id}', 'EmpleadosController@createPdf');

/*
 |------------------------------------
 |          RepuestosController       |
 |------------------------------------
 */

// Ruta buscar para el controlador RepuestosController
Route::post('/repuestos/search', 'RepuestosController@search');
// Ruta para descargar el pdf para el controlador  RepuestosController
Route::get('/repuestos/pdf/{id}', 'RepuestosController@createPdf');

/*
 |------------------------------------
 |          Tipo_Presentacion       |
 |------------------------------------
 */

// Ruta buscar para el controlador Tipo_presentacionController
Route::post('/presentacion/search', 'Tipo_PresentacionController@search');

/*
 |------------------------------------
 |          Salida_Repuestos         |
 |------------------------------------
 */

// Ruta buscar para el controlador Tipo_presentacionController
Route::post('/salida-repuestos/search', 'Salida_RepuestosController@search');

/*
 |------------------------------------
 |          UnidadesController       |
 |------------------------------------
 */

// Ruta buscar para el controlador UnidadesController
Route::post('/unidades/search', 'UnidadesController@search');
// Ruta para descargar el pdf para el controlador  UnidadesController
Route::get('/unidades/pdf/{id}', 'UnidadesController@createPdf');

/*
 |------------------------------------
 |          CargosController       |
 |------------------------------------
 */

// Ruta para descargar el pdf para el controlador  CargosController
Route::get('/cargos/pdf/{id}', 'CargosController@createPdf');

/*
 |------------------------------------
 |          DescuentoController       |
 |------------------------------------
 */

// Ruta para descargar el pdf para el controlador  DescuentoController
Route::get('/descuento/pdf/{id}', 'DescuentoController@createPdf');

/*
 |-------------------------------------------------
 |          Asignacion_DescuentosController       |
 |-------------------------------------------------
 */

// Ruta para descargar el pdf para el controlador  Asignacion_DescuentosController
Route::post('/asignacion-descuentos/search', 'Asignacion_DescuentosController@search');
// Ruta para descargar el pdf para el controlador  Asignacion_DescuentosController
Route::get('/asignacion-descuentos/pdf/{id}', 'Asignacion_DescuentosController@createPdf');
