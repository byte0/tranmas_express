<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidades extends Model
{
    //se define el nombre de la tabla que hace referencia el modelo
    protected $table = 'unidades';
}
