<?php

namespace App\Http\Controllers;

use PDF;              //------>agregando libreria que permite generar los pdf
use App\Cargos;
use App\Unidades;
use App\Empleados;
use Illuminate\Http\Request;

class EmpleadosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //devuelve lista de empleados
        $empleados = Empleados::all();
        $cargos = Cargos::all();
        $unidades = Unidades::all();
        return view("empleados.index")->with(compact('empleados','cargos','unidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //devuelve el formulario para nuevo motorista
        $cargos = Cargos::all();
        $unidades = Unidades::all();
        return view("empleados.create")->with(compact('cargos','unidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validando datos obtenidos del formulario create

        $validatedData = $request->validate([

            'nit' => 'required|alpha_dash|unique:empleados,nit',
            'dui' => 'required|alpha_dash|unique:empleados,dui',
            'fecha_nacimiento' => 'required|date_format:Y-m-d',
            'telefono' => 'required|alpha_dash|numeric',
            'ingreso' => 'required|date_format:Y',
            'estado_civil' => 'required|numeric',
            'isss' => 'nullable|max:35|numeric',
            'afp' => 'nullable|max:35|numeric',
            'direccion' => 'required|max:100',
            'id_unidad' => 'numeric|nullable',
            'apellidos' => 'required|max:35',
            'id_cargo' => 'numeric|nullable',
            'nombres' => 'required|max:35',

        ]);

        //metodo para guardar motoristas

        $empleado = new Empleados();

        $empleado->fecha_nacimiento = $request->input('fecha_nacimiento');
        $empleado->estado_civil = $request->input('estado_civil');
        $empleado->apellidos = $request->input('apellidos');
        $empleado->direccion = $request->input('direccion');
        $empleado->id_unidad = $request->input('id_unidad');
        $empleado->telefono = $request->input('telefono');
        $empleado->id_cargo = $request->input('id_cargo');
        $empleado->ingreso = $request->input('ingreso');
        $empleado->nombres = $request->input('nombres');
        $empleado->isss = $request->input('isss');
        $empleado->dui = $request->input('dui');
        $empleado->nit = $request->input('nit');
        $empleado->afp = $request->input('afp');

        $empleado->save();

        return redirect('/empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleados  $motoristas
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleados  $motoristas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //devuelve formulario de editar
        $empleado = Empleados::find($id);
        $unidadactual = Unidades::find($empleado->id_unidad);
        $cargoactual = Cargos::find($empleado->id_cargo);

        $cargos = Cargos::all();
        $unidades = Unidades::all();

        return view("empleados.edit")->with(compact('empleado','cargos','unidades','cargoactual','unidadactual'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleados  $motoristas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validando datos obtenidos del formulario edit

        $validatedData = $request->validate([

            'fecha_nacimiento' => 'required|date_format:Y-m-d',
            'telefono' => 'required|alpha_dash|numeric',
            'ingreso' => 'required|date_format:Y',
            'estado_civil' => 'required|numeric',
            'id_unidad' => 'numeric|nullable',
            'id_cargo' => 'numeric|nullable',
            'nit' => 'required|alpha_dash',
            'dui' => 'required|alpha_dash',
            'isss' => 'nullable|numeric',
            'afp' => 'nullable|numeric',
            'apellidos' => 'required',
            'direccion' => 'required',
            'nombres' => 'required',

        ]);

        //ejecuta la modificacion de un motorista

        $empleado = Empleados::find($id);
        $empleado->dui = $request->input('dui');
        $empleado->nombres = $request->input('nombres');
        $empleado->apellidos = $request->input('apellidos');
        $empleado->nit = $request->input('nit');
        $empleado->afp = $request->input('afp');
        $empleado->isss = $request->input('isss');
        $empleado->estado_civil = $request->input('estado_civil');
        $empleado->fecha_nacimiento = $request->input('fecha_nacimiento');
        $empleado->telefono = $request->input('telefono');
        $empleado->ingreso = $request->input('ingreso');
        $empleado->direccion = $request->input('direccion');
        $empleado->id_cargo = $request->input('id_cargo');
        $empleado->id_unidad = $request->input('id_unidad');
        $empleado->estado = $request->input('estado');

        $empleado->save();

        return redirect('/empleados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleados  $motoristas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //elimina un motorista
        $empleados = Empleados::find($id);
        $empleados->delete();
        return redirect('/empleados');
    }
    public function search(Request $request)

    {   $cargos = Cargos::all();
        $unidades = Unidades::all();

        $bus = $request->input('bus');
        $buscargos = $request->input('buscargos');

        if ($buscargos > 0  )
        {
            $empleados = Empleados::where('id_cargo', $buscargos)
                ->get();
            $cargobuscado = Cargos::find($buscargos);
            $bus = $cargobuscado->nombre_cargo;
            return view("empleados.search")->with(compact('empleados','cargos','unidades', 'bus'));

        }
        else{
            $empleados = Empleados::where('nombres', 'like', '%' . $bus . '%')
                ->orWhere('apellidos', 'like', '%' . $bus . '%')
                ->orWhere('dui', $bus )
                ->orWhere('nit', $bus )
                ->get();

            return view("empleados.search")->with(compact('empleados', 'cargos','unidades', 'bus'));

        }
    }
    public function createPdf($id)
    {

        $empleado = Empleados::find($id);

        $cargoactual = Cargos::find($empleado->id_cargo);
        $unidadactual = Unidades::find($empleado->id_unidad);

        $pdf = PDF::loadView('empleados.pdf', compact('empleado','cargoactual','unidadactual'));

        return $pdf->download('invoice.pdf');
    }
}
