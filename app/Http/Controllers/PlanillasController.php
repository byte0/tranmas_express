<?php

namespace App\Http\Controllers;

use App\Planillas;
use Illuminate\Http\Request;
use App\Empleados;
use App\Asignacion_Descuentos;
use App\Cargos;
use App\Descuento;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class PlanillasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $planillas = Planillas::all();
        $empleados = Empleados::all();
        return view('planillas.index')->with(compact('planillas','empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //prueba con carbon, obteniendo fecha actual

//        $meses = ['01','02','03','04','05','06','09','08',];
       $fecha_actual = Carbon::now()->format('m');
        //$fecha2 = $fecha_actual->month;
//        foreach ($meses as $mess)
//        {
//           if ($meses)
//           {
//
//           }
//        }
        if (9 == $fecha_actual)
        {
            return "son iguales" . " " .  $fecha_actual;

        }
        else
        {
            return "no son iguales";

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         /*--------------------------------------------------------------------------------------------------------
         * Lamayoria del codigo de este metodo esta comentado al lado derecho para mantener el orden en la lineas |
         * ------------------------------------------------------------------------------------------------------*/

         $empleados_existentes = Empleados::where('estado','=', 0)
                                        ->orWhere('estado','=', 2)
                                        ->get();                                                            //$empleados_existentes trae la lista de empleados
        foreach ($empleados_existentes as $empleados_existente)
        {
            $cargo = Cargos::find($empleados_existente->id_cargo);

            $descuentos_dolares = Asignacion_Descuentos::where('id_empleados', $empleados_existente->id)     //trae la lista de descuento que se aplican en dolares
                                                        ->where('forma_descuento', 2 )
                                                        ->where('cuota_mes', null)
                                                        ->where('aplicabilidad','=',1)
                                                        ->get();
            $descuentos_porcentajes = Asignacion_Descuentos::where('id_empleados', $empleados_existente->id)  //trae la lista de descuentos que se aplican en porcentage
                                                            ->where('forma_descuento', 1 )
                                                            ->where('cuota_mes', null)
                                                            ->where('aplicabilidad','=',1)
                                                            ->get();


            $descuentos_dolares_por_fechas = DB::table('asignacion_descuentos')
                ->whereNotNull('cuota_mes')
                ->where('forma_descuento','=', 2 )
                ->where('id_empleados', '=', $empleados_existente->id)
                ->where('aplicabilidad','=',1)
                ->get();
            $descuentos_procentajes_por_fechas = DB::table('asignacion_descuentos')
                ->whereNotNull('cuota_mes')
                ->where('forma_descuento','=', 1 )
                ->where('id_empleados', '=', $empleados_existente->id)
                ->where('aplicabilidad','=',1)
                ->get();


            $descuento_dolares_actuales = 0;                                                                  //contador para dolares
            foreach ($descuentos_dolares as $descuento_dolar)
            {
                $descuento_dolares_actuales = $descuento_dolares_actuales + $descuento_dolar->valor;          //almacena total de todos los decuentos asignados en dolares al empleado
            }
            $descuento_porcentaje_actuales = 0;                                                               //contador para porcentages
            foreach ($descuentos_porcentajes as $descuentos_porcentaje)
            {
                $descuento_porcentaje_actuales =                                                              //almacena total de todos los descuentos asignados en porcentages
                    $descuento_porcentaje_actuales + ($cargo->sueldo_bruto * $descuentos_porcentaje->valor);
            }
             /*----------------------------------------------------------------------------------------------
             * el siguiente codigo, calcula los descuentos basados en una fecha de inicio y una final, tanto |
             * los descuentos basados en dolares como los basados en porcentajes.                            |
             *                                                                                               |
             *cuando la fecha_final encontrada es menor a la fecha actual, ese descuento deja de ser         |
             * aplicada, ya su tiempo de vigencia a finalizado                                               |
             * ----------------------------------------------------------------------------------------------*/

             $descuento_dolares_por_fecha_actuales = 0;                                                        //contador para dolares con fecha de inicio y final

            foreach ($descuentos_dolares_por_fechas as $desc_fecha)
            {
                $fecha_actual = Carbon::now()->format('m');

                if ($fecha_actual == $desc_fecha->mes_inicial or $fecha_actual == $desc_fecha->mes_final)
                {
                    $descuento_dolares_por_fecha_actuales = $descuento_dolares_por_fecha_actuales + $desc_fecha->cuota_mes;
                }
                elseif ($fecha_actual > $desc_fecha->mes_final)
                {
                    echo "la fecha del descuento ocacional ya caduco";
                    $descuentosupdate = Asignacion_Descuentos::find($desc_fecha->id);
                    $descuentosupdate->aplicabilidad = 0;
                    $descuentosupdate->save();
                }
            }

            $descuento_porsentajes_por_fecha_actuales = 0;                                                         //contador para dolares con fecha de inicio y final

            foreach ($descuentos_procentajes_por_fechas as $desc_porcentaje_fecha)
            {
                $fecha_actual = Carbon::now()->format('m');

                if ($fecha_actual == $desc_porcentaje_fecha->mes_inicial or $fecha_actual == $desc_porcentaje_fecha->mes_final)
                {
                    $descuento_porsentajes_por_fecha_actuales =                                                    //almacena total de todos los descuentos asignados en porcentages
                        $descuento_porsentajes_por_fecha_actuales + ($cargo->sueldo_bruto * $desc_porcentaje_fecha->cuota_mes);
                }
                elseif ($fecha_actual > $desc_porcentaje_fecha->mes_final)
                {
                    echo "la fecha del descuento ocacional ya caduco";
                    $descuentosupdate = Asignacion_Descuentos::find($desc_porcentaje_fecha->id);
                    $descuentosupdate->aplicabilidad = 0;
                    $descuentosupdate->save();
                }
            }
            //codigo que calcula el bono que recibe un empleado si esta de vacaciones
//            $descuento_vacaciones = 0;
//            if ($empleados_existente->estado == 2)
//            {
//                $descuento_vacaciones = 0.15 * ($cargo->sueldo_bruto / 2);
//                echo '</br>';
//                echo 'bono por vacaciones : ' . $descuento_vacaciones;
//            }


            $total_descuentos = $descuento_dolares_actuales + $descuento_porcentaje_actuales +                //almacena la sumatoria de los descuentos
                $descuento_dolares_por_fecha_actuales + $descuento_porsentajes_por_fecha_actuales;

            $sueldo_neto = $cargo->sueldo_bruto - $total_descuentos;                                          //alamcena el sueldo neto que recibira el empleado

            /*-----------------------------------
             * Incertando en la base de datos    |
             * ---------------------------------*/
            $mes_actual = Carbon::now()->format('m');                                                //obteniendo numero del mes actual
            $anio_actual = Carbon::now()->format('y');                                               //obteniendo el año  actual

            $planillas = new Planillas();
            $planillas->id_empleado = $empleados_existente->id;
            $planillas->sueldo_bruto = $cargo->sueldo_bruto;
            $planillas->total_descuentos = $total_descuentos;
            $planillas->sueldo_neto = $sueldo_neto;
            $planillas->mes = $mes_actual;
            $planillas->anio = $anio_actual;
            $planillas->save();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Planillas  $planillas
     * @return \Illuminate\Http\Response
     */
    public function show(Planillas $planillas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Planillas  $planillas
     * @return \Illuminate\Http\Response
     */
    public function edit(Planillas $planillas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Planillas  $planillas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Planillas $planillas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Planillas  $planillas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Planillas $planillas)
    {
        //
    }
}
