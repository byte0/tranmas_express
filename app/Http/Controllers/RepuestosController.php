<?php

namespace App\Http\Controllers;

use App\Repuestos;
use App\Estante;
use App\User;
use PDF;
use App\Tipo_Presentacion;
use Illuminate\Http\Request;

class RepuestosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retorna lista de repuestos

        $repuestos = Repuestos::all();
        $users = User::all();
        $estantes = Estante::all();
        $tipo_presentaciones = Tipo_Presentacion::all();
        return view('repuestos.index')->with(compact('repuestos','users','estantes','tipo_presentaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // devuelve formulario para regustrar prducto o repuesto

        $estantes = Estante::all();
        $tipo_presentaciones = Tipo_Presentacion::all();
        return view('repuestos.create')->with(compact('estantes','tipo_presentaciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       //  return $request->all();

        //metodo para guardar repuespo o productos
        $repuestos = new Repuestos();
        $repuestos->cantidad = $request->input('cantidad');
        $repuestos->codigo_producto = $request->input('codigo_producto');
        $repuestos->descripcion = $request->input('descripcion');
        $repuestos->fecha_de_compra = $request->input('fecha_de_compra');
        $repuestos->fecha_ingreso = $request->input('fecha_ingreso');
        $repuestos->id_usuario = $request->input('ingresado_por');
        $repuestos->id_estante = $request->input('numero_estante');
        $repuestos->id_tipo_presentacion = $request->input('presentacion');
        $repuestos->nombre_producto = $request->input('nombre_producto');
        $repuestos->numero_factura = $request->input('numero_factura');
        $repuestos->precio = $request->input('precio');
        $repuestos->save();

        return redirect('/repuestos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Repuestos  $repuestos
     * @return \Illuminate\Http\Response
     */
    public function show(Repuestos $repuestos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Repuestos  $repuestos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //formulario para editar
        $repuestos = Repuestos::find($id);

        $estante_actual = Estante::find($repuestos->id_estante);
        $tipo_presentaciones_actual = Tipo_Presentacion::find($repuestos->id_tipo_presentacion);

        $estantes = Estante::all();
        $tipo_presentaciones = Tipo_Presentacion::all();

        return view('repuestos.edit')->with(compact('repuestos','estantes','tipo_presentaciones','estante_actual','tipo_presentaciones_actual'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Repuestos  $repuestos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $repuestos = Repuestos::find($id);
        $repuestos->cantidad = $request->input('cantidad');
        $repuestos->codigo_producto = $request->input('codigo_producto');
        $repuestos->descripcion = $request->input('descripcion');
        $repuestos->fecha_de_compra = $request->input('fecha_de_compra');
        $repuestos->fecha_ingreso = $request->input('fecha_ingreso');
        $repuestos->id_usuario = $request->input('ingresado_por');
        $repuestos->id_estante = $request->input('numero_estante');
        $repuestos->id_tipo_presentacion = $request->input('presentacion');
        $repuestos->nombre_producto = $request->input('nombre_producto');
        $repuestos->numero_factura = $request->input('numero_factura');
        $repuestos->precio = $request->input('precio');
        $repuestos->save();

        return redirect('/repuestos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Repuestos  $repuestos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //metodo para eliminar un registro
        $repuestos = Repuestos::find($id);
        $repuestos->delete();
        return redirect('/repuestos');
    }
    public function search(Request $request)
    {
        //metodo encargado de realizar busquedas

        $users = User::all();
        $estantes = Estante::all();
        $tipo_presentaciones = Tipo_Presentacion::all();

        $bus = $request->input('bus');

        $repuestos = Repuestos::where('nombre_producto', 'like', '%' . $bus . '%')
                                ->orWhere('codigo_producto', $bus)
                                ->orWhere('numero_factura', $bus )
                                ->get();

        return view("repuestos.search")->with(compact('repuestos', 'bus','users','estantes','tipo_presentaciones'));

    }

    public function createPdf($id)
    {
        $users = User::all();
        $estantes = Estante::all();
        $tipo_presentaciones = Tipo_Presentacion::all();

        $repuestos = Repuestos::find($id);

        $pdf = PDF::loadView('repuestos.create_pdf', compact('repuestos','users','estantes','tipo_presentaciones'));

        return $pdf->download('invoice.pdf');
    }
}
