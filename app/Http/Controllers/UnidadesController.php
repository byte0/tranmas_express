<?php

namespace App\Http\Controllers;

use App\Unidades;
use Hamcrest\Core\AllOf;
use Illuminate\Http\Request;
use PDF;              //------>agregando libreria que permite generar los pdf

class UnidadesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retorna una vista
        $unidades = Unidades::all();
        return view('unidades.index')->with(compact('unidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //retorna formulario para registrar nueva unidad
        return view('unidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $unidades = new Unidades();
        $unidades->nombre_unidad = $request->input('nombre_unidad');
        $unidades->numero_placa = $request->input('numero_placa');
        $unidades->descripcion = $request->input('descripcion');
        $unidades->estado = $request->input('estado');
        $unidades->save();

        return redirect('/unidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function show(Unidades $unidades)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $unidad = Unidades::find($id);
        return view('unidades.edit')->with(compact('unidad'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //realiza un update en un registro
        $unidades = Unidades::find($id);
        $unidades->nombre_unidad = $request->input('nombre_unidad');
        $unidades->numero_placa = $request->input('numero_placa');
        $unidades->descripcion = $request->input('descripcion');
        $unidades->estado = $request->input('estado');
        $unidades->save();

        return redirect('/unidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $unidades = Unidades::find($id);
        $unidades->delete();
        return redirect('/unidades');
    }

    public function search(Request $request)
    {
        $bus = $request->input('bus');

        $unidades = Unidades::where('nombre_unidad', 'like', '%' . $bus . '%')
            ->orWhere('numero_placa', $bus)
            ->get();

        return view("unidades.search")->with(compact('unidades', 'bus'));
    }
    //imprime pdf de una unidad
    public function createPdf($id)
    {

        $unidad = Unidades::find($id);

        $pdf = PDF::loadView('unidades.pdflist', compact('unidad'));

        return $pdf->download('unidades.pdf');
    }

}
