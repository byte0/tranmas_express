<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//agregando el modelo
use App\Descuento;
use App\Tipo_Descuentos;
use PDF;              //------>agregando libreria que permite generar los pdf


class DescuentoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $descuentos = Descuento::all();
       $tipo_descuentos = Tipo_Descuentos::all();
       return view('descuentos.index')->with(compact('descuentos','tipo_descuentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //probando el render
        $tipo_descuentos = Tipo_Descuentos::all();
        return view('descuentos.crear')->with(compact('tipo_descuentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validando datos obtenidos del formulario create

        $validatedData = $request->validate([
            'nombre_descuento' => 'required|max:255',
            'fecha_asignacion' => 'required|date_format:Y-m-d',
            'idtipodescuento' => 'required',
        ]);

        //almacenando los valores en la base de datos

        $descuentos = new Descuento();
        $descuentos->nombre_descuento = $request->input('nombre_descuento');
        $descuentos->fecha_asignacion = $request->input('fecha_asignacion');
        $descuentos->idtipodescuento = $request->input('idtipodescuento');
        $descuentos->activo = $request->input('activo');
        $descuentos->save();
        return redirect('/descuento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //buscando un descuento para editarlo
        $tipo_descuentos = Tipo_Descuentos::all();
        $descuento = Descuento::find($id);
        return view("descuentos.editar")->with(compact('descuento','tipo_descuentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validando datos obtenidos del formulario edit

        $validatedData = $request->validate([
            'nombre_descuento' => 'required|max:255',
            'fecha_asignacion' => 'required|date_format:Y-m-d',
            'tipo_descuento' => 'required',
        ]);

        //actualizano el registro del descuento

        $descuento = Descuento::find($id);
        $descuento->nombre_descuento = $request->input('nombre_descuento');
        $descuento->fecha_asignacion = $request->input('fecha_asignacion');
        $descuento->idtipodescuento = $request->input('tipo_descuento');
        $descuento->save();
        return redirect('/descuento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //eliminando un descuento
        $descuento = Descuento::find($id);
        $descuento->delete();
        return redirect('/descuento');
    }
    public function createPdf()
    {

        $descuentos = Descuento::all();
        $tipo_descuentos = Tipo_Descuentos::all();
        $pdf = PDF::loadView('descuentos.pdflist', compact('descuentos','tipo_descuentos'));

        return $pdf->download('Lista_Descuentos.pdf');
    }
}
