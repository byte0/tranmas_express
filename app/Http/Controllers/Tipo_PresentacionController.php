<?php

namespace App\Http\Controllers;

use App\Tipo_Presentacion;
use Illuminate\Http\Request;

class Tipo_PresentacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retorna lista de tipos de presentacion de reouestos
        $presentaciones = Tipo_Presentacion::all();
        return view('tipo_presentacion.index')->with(compact('presentaciones'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //retorna el formulario de create

        return view('tipo_presentacion.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //metodo para guardar repuespo o productos
        $presentacion = new Tipo_Presentacion();
        $presentacion->presentacion = $request->input('presentacion');
        $presentacion->unidad_medida = $request->input('unidad_medida');
        $presentacion->save();

        return redirect('/presentacion');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tipo_Presentacion  $tipo_Presentacion
     * @return \Illuminate\Http\Response
     */
    public function show(Tipo_Presentacion $tipo_Presentacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tipo_Presentacion  $tipo_Presentacion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // devuelve el formulario para editar
        $presentacion = Tipo_Presentacion::find($id);

        return view('tipo_presentacion.edit')->with(compact('presentacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tipo_Presentacion  $tipo_Presentacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //se encarga de guardar los cambios en la db

        $presentacion = Tipo_Presentacion::find($id);
        $presentacion->presentacion = $request->input('presentacion');
        $presentacion->unidad_medida = $request->input('unidad_medida');
        $presentacion->save();

        return redirect('/presentacion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tipo_Presentacion  $tipo_Presentacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //se encarga de eliminar un tiipo de presentacion

        $presentacion = Tipo_Presentacion::find($id);
        $presentacion->delete();
        return redirect('/presentacion');

    }

    public function search(Request $request)
    {
        $bus = $request->input('bus');

        $presentaciones = Tipo_Presentacion::where('presentacion', 'like', '%' . $bus . '%')
            ->orWhere('unidad_medida', $bus)
            ->get();

        return view("tipo_presentacion.search")->with(compact('presentaciones', 'bus'));

    }
}
