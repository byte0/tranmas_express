<?php

namespace App\Http\Controllers;

use App\Salida_Repuestos;
use App\Repuestos;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function PHPSTORM_META\elementType;

class Salida_RepuestosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retorna lista de salida de repuestos

        $users = User::all();
        $repuestos = Repuestos::all();
        $salida_repuestos = Salida_Repuestos::all();

        return view('salida_repuestos.index')->with(compact('salida_repuestos','repuestos','users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //retorna formulario para agregar una nueva salida de repuestos del invenntario

        $repuestos = Repuestos::all();
        return view('salida_repuestos.create')->with(compact('repuestos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $repuesto = Repuestos::find($request->input('id_repuesto'));
        $cantidaddb = $repuesto->cantidad;

        $cantidadinput = $request->input('cantidad_entregada');

        if ($cantidaddb >= $cantidadinput and $cantidadinput >= 1)//se evalua si la cantidad de salida es menor o igual
        {                                                        //que la existen en la db si es asi sigue proceso a guardar
           //cantidad a restar en repuestos
            $cantidad_a_guardar = $cantidaddb - $cantidadinput;

            //proceso para guardar la resta de la cantidad de respuestos salientes de la tabla repuestos
            $repuesto = Repuestos::find($request->input('id_repuesto'));
            $repuesto->cantidad = $cantidad_a_guardar;
            $repuesto->save();

            //proceso para guardar un registro de la salida de repuestos
            $salida = new Salida_Repuestos();
            $salida->idrespuesto = $request->input('id_repuesto');
            $salida->idusuario_entrega = $request->input('id_usuario_entreda');
            $salida->idusuario_recibe = $request->input('id_usuario_recibe');
            $salida->cantidad_entregada = $request->input('cantidad_entregada');
            $salida->fecha_salida = $request->input('fecha_de_salida');
            $salida->save();
            return redirect('/salida-repuestos');
        }
        else
        {
            $error = "store";
            return view('salida_repuestos.error')->with(compact('error'));        }
        }

    /**
     * Displhttps://ngrok.com/downloaday the specified resource.
     *
     * @param  \App\Salida_Repuestos  $salida_Repuestos
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //

     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salida_Repuestos  $salida_Repuestos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $repuestos = Repuestos::all();
        $salida_repuesto = Salida_Repuestos::find($id);
        $repuestoname = Repuestos::find($salida_repuesto->idrespuesto);

        return view('salida_repuestos.edit')->with(compact('salida_repuesto','repuestos','repuestoname'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salida_Repuestos  $salida_Repuestos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //proceso para modificar un registro de salida de repuestos

        $repuestos = Repuestos::find($request->input('id_repuesto'));
        $salida = Salida_Repuestos::find($id);
        /*--------------------------------------------------------------------------------------------------------*/
        //la cantidad del formulario es mayor a la cantidad que hay actualmente en Salida_Repuestos
        if ($request->input('cantidad_entregada') > $salida->cantidad_entregada)
        {
            //$cantidad_complemento contiene la diferencia del cantindad ingresada y la cantidad actual
            // del registo en Salida_Repuestos
           $cantidad_complemento = $request->input('cantidad_entregada') - $salida->cantidad_entregada;

            //si la cantidad de respuestos es mayor o igual a la cantidad de complemento
           if ($repuestos->cantidad >= $cantidad_complemento)
           {
               //guarda en la tabla de repuestos, restadole la cantidad de salida
               $h = $repuestos->cantidad - $cantidad_complemento;
               $repuestos->cantidad = $h;
               $repuestos->save();

               //ejecuta el update en la tabla Salida_Repuestos
                $salida = Salida_Repuestos::find($id);
                $salida->idrespuesto = $request->input('id_repuesto');
                $salida->idusuario_recibe = $request->input('id_usuario_recibe');
                $salida->cantidad_entregada = $request->input('cantidad_entregada');
                $salida->fecha_salida = $request->input('fecha_de_salida');
                $salida->save();

                return redirect('/salida-repuestos');
           }
           else{
               $error = "update";
               return view('salida_repuestos.error')->with(compact('error'));
           }
        }
        /*--------------------------------------------------------------------------------------------------------*/
        //la cantidad del formulario es menor a la cantidad actualmente en Salida_Repuestos
        elseif($request->input('cantidad_entregada') < $salida->cantidad_entregada)
        {
            $cantidad_sobrante =  $salida->cantidad_entregada - $request->input('cantidad_entregada');

            //guarda en la tabla de repuestos, sumando la cantidad de salida que es menor a la anterior
            $h = $repuestos->cantidad + $cantidad_sobrante;
            $repuestos->cantidad = $h;
            $repuestos->save();

            //ejecuta el update en la tabla Salida_Repuestos
            $salida = Salida_Repuestos::find($id);
            $salida->idrespuesto = $request->input('id_repuesto');
            $salida->idusuario_recibe = $request->input('id_usuario_recibe');
            $salida->cantidad_entregada = $request->input('cantidad_entregada');
            $salida->fecha_salida = $request->input('fecha_de_salida');
            $salida->save();

            return redirect('/salida-repuestos');
        }
        //la cantidad del formulario es igual a la cantidad actualmente en Salida_Repuestos
        /*--------------------------------------------------------------------------------------------------------*/

        elseif($request->input('cantidad_entregada') == $salida->cantidad_entregada)
        {
            $cantidad_igual =  $salida->cantidad_entregada - $request->input('cantidad_entregada');

            //guarda en la tabla de repuestos, restadole la cantidad de salida en este caso 0
            $h = $repuestos->cantidad + $cantidad_igual;
            $repuestos->cantidad = $h;
            $repuestos->save();

            //ejecuta el update en la tabla Salida_Repuestos
            $salida = Salida_Repuestos::find($id);
            $salida->idrespuesto = $request->input('id_repuesto');
            $salida->idusuario_recibe = $request->input('id_usuario_recibe');
            $salida->cantidad_entregada = $request->input('cantidad_entregada');
            $salida->fecha_salida = $request->input('fecha_de_salida');
            $salida->save();

            return redirect('/salida-repuestos');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salida_Repuestos  $salida_Repuestos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //metodo para eliminar un registro
        $salida = Salida_Repuestos::find($id);
        $salida->delete();
        return redirect('/salida-repuestos');
    }
    public function search(Request $request)
    {
        $bus = $request->input('bus');
       // $repuestos = Repuestos::where('nombre_producto', $request->input('bus'))->get();
        $repuestos = Repuestos::all();
        foreach ($repuestos as $repuesto)
        {
            if($repuesto->nombre_producto == $request->input('bus'))
            {
                $salida_repuestos = Salida_Repuestos::where('idrespuesto',$repuesto->id)->get();
                $users = User::all();
                return view('salida_repuestos.search')->with(compact('salida_repuestos','repuestos','users','bus'));
            }
        }
        $salida_repuestos = 0;
        return view('salida_repuestos.search')->with(compact('salida_repuestos','bus'));
    }
}
