<?php

namespace App\Http\Controllers;

use App\Cargos;
use Illuminate\Http\Request;
use PDF;              //------>agregando libreria que permite generar los pdf

class CargosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //devuelve lista de cargos
        $cargos = Cargos::all();
        return view('cargos.index')->with(compact('cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //devuelve formulario para crear nuevo cargo
        return view('cargos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validando datos obtenidos del formulario create

        $validatedData = $request->validate([
            'nombre_cargo' => 'required|max:30',
            'descripcion' => 'required|max:255',
            'sueldo_bruto' => 'numeric',
        ]);

        //guarda los datos que vienen del formulario create

        $cargos = new Cargos();
        $cargos->nombre_cargo = $request->input('nombre_cargo');
        $cargos->descripcion = $request->input('descripcion');
        $cargos->sueldo_bruto = $request->input('sueldo_bruto');
        $cargos->save();

        return redirect('/cargos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function show(Cargos $cargos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //mostrara formulario para editar
        $cargo = Cargos::find($id);
        return view('cargos.edit')->with(compact('cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validando datos obtenidos del formulario edit

        $validatedData = $request->validate([
            'nombre_cargo' => 'required|max:30',
            'descripcion' => 'required|max:255',
            'sueldo_bruto' => 'numeric',
        ]);

        //guarda los datos que vienen del formulario edit

        $cargos = Cargos::find($id);
        $cargos->nombre_cargo = $request->input('nombre_cargo');
        $cargos->descripcion = $request->input('descripcion');
        $cargos->sueldo_bruto = $request->input('sueldo_bruto');
        $cargos->save();

        return redirect('/cargos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //elimina un cargo
        $cargos = Cargos::find($id);
        $cargos->delete();

        return redirect('/cargos');
    }
    public function createPdf()
    {

        $cargos = Cargos::all();

        $pdf = PDF::loadView('cargos.pdflist', compact('cargos'));

        return $pdf->download('cargos.pdf');
    }
}
