<?php

namespace App\Http\Controllers;

use App\Asignacion_Descuentos;
use Illuminate\Http\Request;
use App\Empleados;
use App\Descuento;
use App\Meses;
use PDF;              //------>agregando libreria que permite generar los pdf

class Asignacion_DescuentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $asignacion_descuentos = Asignacion_Descuentos::all();
        $empleados = Empleados::all();
        $descuentos = Descuento::all();

        return view('asignacion_descuentos.index')->with(compact('asignacion_descuentos','empleados','descuentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        $empleados = Empleados::all();
        $descuentos = Descuento::all();
        $meses = Meses::all();

        return view('asignacion_descuentos.create')->with(compact('empleados','descuentos',
            'meses'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //guarda los datos que vienen del formulario create
        $signacion_desc = new Asignacion_Descuentos();
        $signacion_desc->id_empleados = $request->input('id_empleado');
        $signacion_desc->id_descuentos = $request->input('id_descuento');
        $signacion_desc->valor = $request->input('valor');
        $signacion_desc->forma_descuento =$request->input('forma_descuento');
        $signacion_desc->aplicabilidad = 1;
        $signacion_desc->fecha_asignacion = $request->input('fecha_asignacion');
        if ($request->input('cuota_mes') > 0)
        {
            $signacion_desc->mes_inicial = $request->input('mes_inicio');
            $signacion_desc->mes_final = $request->input('mes_final');
            $signacion_desc->cuota_mes = $request->input('cuota_mes');
        }
        else {

            $signacion_desc->mes_inicial = 0;
            $signacion_desc->mes_final = 0;
            $signacion_desc->cuota_mes = $request->input('cuota_mes');
        }
        $signacion_desc->save();

        return redirect('/asignacion-descuentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AsignacionDescuentos  $asignacionDescuentos
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AsignacionDescuentos  $asignacionDescuentos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $asignacion_desc = Asignacion_Descuentos::find($id);
        $empleados = Empleados::all();
        $descuentos = Descuento::all();
        return view('asignacion_descuentos.edit')->with(compact('asignacion_desc','empleados',
            'descuentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AsignacionDescuentos  $asignacionDescuentos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $signacion_desc = Asignacion_Descuentos::find($id);
        $signacion_desc->id_empleados = $request->input('id_empleado');
        $signacion_desc->id_descuentos = $request->input('id_descuento');
        $signacion_desc->valor = $request->input('valor');
        $signacion_desc->forma_descuento =$request->input('forma_descuento');
        $signacion_desc->aplicabilidad = 1;
        $signacion_desc->fecha_asignacion = $request->input('fecha_asignacion');
        $signacion_desc->save();

        return redirect('/asignacion-descuentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AsignacionDescuentos  $asignacionDescuentos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //elimina una asignacion
        $signacion_desc = Asignacion_Descuentos::find($id);
        $signacion_desc->delete();

        return redirect('/asignacion-descuentos');
    }
    public function search(Request $request)
    {
        $empleadosall = Empleados::all();
        $descuentos = Descuento::all();

        $bus = $request->input('bus');

        $empleados = Empleados::where('apellidos', $bus )->get();

        if (count($empleados) > 0)
        {
            foreach ($empleados as $empleado)
            {
                $empleados_con_asignacion = Asignacion_Descuentos::where('id_empleados',$empleado->id)->get();

                return view('asignacion_descuentos.search')->with(compact('empleados_con_asignacion','empleadosall','descuentos','bus'));
            }
        }
        else
        {
            $empleados_con_asignacion = [];

            return view('asignacion_descuentos.search')->with(compact('empleados_con_asignacion','bus'));
        }
    }

    public function createPdf()
    {
        $asignacion_descuentos = Asignacion_Descuentos::all();
        $empleados = Empleados::all();
        $descuentos = Descuento::all();
        $pdf = PDF::loadView('asignacion_descuentos.pdflist', compact('asignacion_descuentos',
            'empleados','descuentos'));
        return $pdf->download('Asignaciones_de_Descuentos.pdf');
    }
}
