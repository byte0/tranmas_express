@extends('layouts.mantenimientos')
@section('title')| Editar Empleado @endsection
@section('content')
    <br><br>
    <div class="white">
        <div class="card-panel">
            <h3 class="">Editando a {{ $empleado->nombres  }}</h3>
            <div class="divider"></div>
            <br>
            <form action="{{ url('/empleados/'. $empleado->id ) }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">credit_card</i>
                        <input  type="text" name="dui" value="{{ $empleado->dui  }}" class="validate" required>
                        <label>Dui</label>
                        @if ($errors->has('dui'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('dui') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">account_circle</i>
                        <input  type="text" name="nombres" value="{{ $empleado->nombres  }}" class="validate" required>
                        <label>Nombres</label>
                        @if ($errors->has('nombres'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('nombres') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">account_circle</i>
                        <input type="text" name="apellidos" value="{{ $empleado->apellidos  }}" class="validate" required>
                        <label>Apellidos</label>
                        @if ($errors->has('apellidos'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('apellidos') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">credit_card</i>
                        <input type="text" name="nit" value="{{ $empleado->nit  }}" class="validate" required>
                        <label>Nit</label>
                        @if ($errors->has('nit'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('nit') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">credit_card</i>
                        <input type="text" name="afp" value="{{ $empleado->afp  }}" class="validate" >
                        <label>AFP</label>
                        @if ($errors->has('afp'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('afp') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6 row">
                        <b class="col s12 center-align">Estado Civil</b>
                        <br>
                        @if ($empleado->estado_civil == 0)
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" name="estado_civil" value="0" checked="checked" class="filled-in" />
                                    <span>Soltero/a</span>
                                </label>
                            </p>
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" name="estado_civil" id="casado"  value="1" class="filled-in"  />
                                    <span>Casado/a</span>
                                </label>
                            </p>
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" name="estado_civil"  value="2" class="filled-in"  />
                                    <span>Viudo/a</span>
                                </label>
                            </p>

                        @elseif ($empleado->estado_civil == 1 )                       <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado_civil" value="0" class="filled-in" />
                                <span>Soltero/a</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado_civil" id="casado" checked="checked" value="1" class="filled-in"  />
                                <span>Casado/a</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado_civil" value="2" class="filled-in"  />
                                <span>Viudo/a</span>
                            </label>
                        </p>
                        @else
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" name="estado_civil" value="0" class="filled-in" />
                                    <span>Soltero/a</span>
                                </label>
                            </p>
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" name="estado_civil"  value="1" class="filled-in"  />
                                    <span>Casado/a</span>
                                </label>
                            </p>
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" name="estado_civil"  checked="checked" value="2" class="filled-in"  />
                                    <span>Viudo/a</span>
                                </label>
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">location_on</i>
                        <input type="text" name="direccion" value="{{ $empleado->direccion  }}" class="validate" required>
                        <label>Direccion</label>
                        @if ($errors->has('direccion'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('direccion') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">local_hospital</i>
                        <input type="text" name="isss" value="{{ $empleado->isss  }}" class="validate" >
                        <label>ISSS</label>
                        @if ($errors->has('isss'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('isss') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">cake</i>
                        <input  type="text" name="fecha_nacimiento" value="{{ $empleado->fecha_nacimiento  }}" class="datepicker" required>
                        <label>Fecha de nacimiento</label>
                        @if ($errors->has('fecha_nacimiento'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">call</i>
                        <input type="tel" name="telefono" value="{{ $empleado->telefono  }}" required>
                        <label>telefono</label>
                        @if ($errors->has('telefono'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">calendar_today</i>
                        <input  type="text" name="ingreso" value="{{ $empleado->ingreso  }}" class="validate" required>
                        <label>Año de ingreso</label>
                        @if ($errors->has('ingreso'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('ingreso') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_cargo" >
                            @if(count($cargoactual) > 0 )
                                <option selected value="{{ $empleado->id_cargo }}">{{ $cargoactual->nombre_cargo }}</option>
                            @else
                                <option selected value="0" >sin cargo</option>
                            @endif
                            @foreach ($cargos as $cargo)
                                <option  value="{{ $cargo->id }}">{{ $cargo->nombre_cargo }}</option>
                            @endforeach
                        </select>
                        <label>Cargo</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_unidad">
                            @if(count($unidadactual) > 0 )
                            <option selected value="{{ $empleado->id_unidad }}">{{ $unidadactual->nombre_unidad }}</option>
                            @else
                                <option selected value="0" >sin unidad</option>
                            @endif
                            @foreach ($unidades as $unidad)
                                <option value="{{ $unidad->id }}">{{ $unidad->nombre_unidad }}</option>
                            @endforeach
                        </select>
                        <label>Unidad</label>
                    </div>
                    <div class="input-field col s12">
                        <b class="center-align col s12">Estado del empleado</b>
                        <br>
                        @if ($empleado->estado == 0)
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" id="empleado" name="estado" value="0" checked="checked" onclick="check1(this)" class="filled-in" />
                                    <span>empleado</span>
                                </label>
                            </p>
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" id="ex_empleado" name="estado"  value="1"  onclick="check2(this)"class="filled-in"  />
                                    <span>Ex empleado</span>
                                </label>
                            </p>
                        @elseif ($empleado->estado == 1 )
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" id="empleado"name="estado" value="0"  onclick="check1(this)" class="filled-in" />
                                    <span>empleado</span>
                                </label>
                            </p>
                            <p class="col s12 l4">
                                <label >
                                    <input type="checkbox" id="ex_empleado" name="estado"  checked="checked" value="1"  onclick="check2(this)" class="filled-in"  />
                                    <span>Ex empleado</span>
                                </label>
                            </p>
                        @else
                        @endif
                    </div>
                </div>
                <div class="row">
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Cambios</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
    <br>
    <script>
        function check1(box)
        {
            if (box.checked) {
                document.getElementById("ex_empleado").checked=false;
            }
        }
        function check2(box) {
            if (box.checked) {
                document.getElementById("empleado").checked=false;
            }

        }
    </script>
@stop

