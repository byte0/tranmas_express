@extends('layouts.templatelarge')
@section('title')| Empleados @endsection
@section('content')
    <form class="col s12 l3 left transparent" action="{{ url('/planillas/') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="input-field col s12">
            <button class="waves-effect waves-light btn teal white-text" type="submit" name="action">
                <i class="material-icons left">autorenew</i>Generar Planilla</button>
        </div>
    </form>
    <br>
<div class="row">
    <div class="card-panel">
        <div class="row ">
            <h3 class="dark-text col l5 center-align"> <b> Listado de Empleados </b></h3>
            <form class="col s12 m12 l4 row right " action="{{ url('/empleados/search') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="input-field col s5 ">
                    <i class="material-icons prefix">folder_open</i>
                    <select name="buscargos" >
                        @foreach ($cargos as $cargo)
                            <option value="{{ $cargo->id }}">{{ $cargo->nombre_cargo }}</option>
                        @endforeach
                    </select>
                    <label>Filtrar Por Cargo</label>
                </div>
                <div class="input-field col s7 ">
                    <button class="col s10 push-s1 btn waves-effect waves-light teal white-text right" type="submit" name="action">Buscar
                    </button>
                </div>
            </form>

        </div>
        <div class="divider"></div>
        <table class="responsive-table white centered " >
            <thead>
            <tr>
                <th>DUI</th>
                <th>Nombre Completo</th>
                <th>Nit</th>
                <th>AFP</th>
                <th>Estado Civil</th>
                <th>ISSS</th>
                <th>Fech.Nac</th>
                <th>Telefono</th>
                <th>Ingreso</th>
                <th>Direccion</th>
                <th>Cargo</th>
                <th>Unidad</th>
                <th>Estado</th>
                <th colspan="3">Acciones</th>
            </tr>
            </thead>

            <tbody>
            @foreach ($empleados as $empleado)
                <tr>
                    <td>{{ $empleado->dui }}</td>
                    <td>{{ $empleado->nombres . " " . $empleado->apellidos }}</td>
                    <td>{{ $empleado->nit }}</td>
                    <td>{{ $empleado->afp }}</td>
                    <td>
                        @if ($empleado->estado_civil == 0)
                            Soltero
                        @elseif ($empleado->estado_civil == 1 )
                            Casado
                        @else
                            Viudo
                        @endif
                    </td>
                    <td>{{ $empleado->isss }}</td>
                    <td>{{ $empleado->fecha_nacimiento }}</td>
                    <td>{{ $empleado->telefono }}</td>
                    <td>{{ $empleado->ingreso }}</td>
                    <td>{{ $empleado->direccion }}</td>
                    <td>
                        @foreach($cargos as $cargo)
                            @if($cargo->id == $empleado->id_cargo)
                                {{ $cargo->nombre_cargo }}
                            @endif
                        @endforeach
                    </td>
                    <td>
                        @foreach($unidades as $unidad)
                            @if($unidad->id == $empleado->id_unidad)
                                {{ $unidad->nombre_unidad }}
                            @endif
                        @endforeach
                    </td>

                    <td>
                        @if ($empleado->estado == 0)
                            empleado
                        @elseif ($empleado->estado == 1 )
                            ex empleado
                        @else
                            De vacaciones
                        @endif
                    </td>
                    <td>
                        <form class="col s12 m12 l12" action="{{ url('empleados'. $empleado->id) }}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn waves-effect waves-light red center tooltipped" type="submit" name="action"
                                    data-position="top" data-tooltip="Eliminar">
                                <i class="material-icons">delete</i>
                            </button>
                        </form>
                    </td>
                    <td>
                        <div class="show-on-small hide-on-large-only"> <br></div>
                        <a href="{{ url('/empleados/'. $empleado->id .'/edit') }}" class="btn btn-success blue tooltipped center"
                           data-position="top" data-tooltip="Editar">
                            <i class="material-icons ">edit</i></a>
                    </td>
                    <td>
                        <div class="show-on-small hide-on-large-only"> <br></div>
                        <a href="{{ url('/empleados/pdf/'. $empleado->id ) }}" class="btn btn-success tooltipped center"
                           data-position="top" data-tooltip="Descargar PDF">
                            <i class="material-icons ">print</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">apps</i>
            </a>
            <ul>
                <li><a class="btn-floating red" href="{{ url('/empleados/create') }}"><i class="material-icons">add</i></a></li>
                <li><a class="btn-floating yellow darken-1 modal-trigger" href="#modal1"><i class="material-icons">search</i></a></li>
            </ul>
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Buscar Motorista</h4>
                    <br>
                    <p>puedes buscar motoristas por DUI & NIT o nombres & apellidos</p>
                    <div class="col s12">
                        <form class="col s12 m12 l12 row" action="{{ url('/empleados/search') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                            <div class="input-field col s12 l12">
                                <i class="material-icons prefix">search</i>
                                <input type="text" name="bus" required>
                                <label>Buscar</label>
                            </div>
                            <div class="input-field col s12">
                                <button class="col s10 push-s1 btn waves-effect waves-light yellow blue-text" type="submit" name="action">Buscar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
@endsection
