<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <div class="lavender">
        <h1 class="center">Tranma Express</h1>
        <label><b>Puesto : </b>@if(empty($cargoactual)) sin cargo @else {{ $cargoactual->nombre_cargo }} @endif</label><br>
        <b>Datos: </b><br>
    </div>
    <div>
        <hr>
        <h5 class="right">Personales</h5>
        <label><b> Nombre :</b> {{ $empleado->nombres . " " . $empleado->apellidos }}</label><br>
        <label><b>Estado Civil :</b>
            @if ($empleado->estado_civil == 0)Soltero
            @elseif ($empleado->estado_civil == 1 )
                Casado
            @else
                Viudo
            @endif
        </label><br>
        <label><b>Telefono :</b>{{ $empleado->telefono }}</label><br>
        <label><b>Direccion :</b>{{ $empleado->direccion }}</label><br>
        <label><b>Fecha de nacimiento :</b>{{ $empleado->fecha_nacimiento }}</label><br>
        <label><b>Unidad :</b>
            @if(empty($unidadactual))
                sin unidad
            @else
                {{ $unidadactual->nombre_unidad }}
            @endif</label><br>
        <hr>
    </div>
    <div>
        <h5 class="right">Adicionales</h5>
        <label><b> Dui :</b> {{ $empleado->dui }}</label><br>
        <label><b>Nit :</b>{{ $empleado->nit }}</label><br>
        <label><b>AFP :</b>{{ $empleado->afp }}</label><br>
        <label><b>ISSS :</b>{{ $empleado->isss }}</label><br>
        <label><b>Ingreso :</b>{{ $empleado->ingreso }}</label><br>
        <hr>
    </div>
    <div class="lavender">

        <h5><b>contactanos</b></h5>
        <label><b>Direccion :</b>
            Lotificación las Victorias 6 Calle Pte Lt 2 – 4
            Sonsonate
        </label><br>
        <label><b>Telefono :</b>2484 5620</label><br>

    </div>
    <style>
        /*
	Color fondo: #632432;
	Color header: 246355;
	Color borde: 0F362D;
	Color iluminado: 369681;
*/
        body{
            background-color: #ffffff;
            font-family: Arial;
            margin: 0px;
            padding: 0px;
        }


        table{
            background-color: white;
            text-align: left;
            border-collapse: collapse;
            width: 100%;
        }

        th, td{
            padding: 1px;
            margin: 1px;
            text-align: center;
            width: 8%;
        }

        thead{
            background-color: darkblue;
            border-bottom: solid 5px cyan;
            color: white;
        }

        tr:nth-child(even){
            background-color: #ddd;
        }

        tr:hover td{
            background-color: #369681;
            color: white;
        }
        .center{
            text-align: center;
        }
        .right
        {
            text-align: center;
            color: blue;
        }
        .lavender
        {
            background-color: #E6E6FA;
            padding: 7px;
        }
    </style>
</body>
</html>

