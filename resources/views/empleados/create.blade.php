@extends('layouts.mantenimientos')
@section('title')| Nuevo Empleado @endsection
@section('content')
    <br><br>
    <div class="white">
        <div class="card-panel">
            <h3 class="center-align">NUEVO EMPLEADO</h3>
            <div class="divider"></div>
            <br>
            <form action="{{ url('/empleados') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">account_circle</i>
                        <input  type="text" name="nombres" value="{{ old('nombres') }}" class="validate" required>
                        <label>Nombres</label>
                        @if ($errors->has('nombres'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('nombres') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">account_circle</i>
                        <input type="text" name="apellidos" value="{{ old('apellidos') }}" class="validate" required>
                        <label>Apellidos</label>
                        @if ($errors->has('Apellidos'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('Apellidos') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">credit_card</i>
                        <input  type="text" name="dui" class="validate" value="{{ old('dui') }}" required>
                        <label>Dui</label>
                        @if ($errors->has('dui'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('dui') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">credit_card</i>
                        <input type="text" name="nit" value="{{ old('nit') }}" class="validate" required>
                        <label>Nit</label>
                        @if ($errors->has('nit'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('nit') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">credit_card</i>
                        <input type="text" name="afp" value="{{ old('afp') }}" class="validate">
                        <label>AFP</label>
                        @if ($errors->has('afp'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('afp') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6 row">
                        <b class="col s12 center-align">Estado Civil</b>
                        <br>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado_civil" id="soltero"  onclick="check1(this)" value="0" class="filled-in" />
                                <span>Soltero/a</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado_civil" id="casado"  onclick="check2(this)" value="1" class="filled-in"  />
                                <span>Casado/a</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado_civil" id="viudo" onclick="check3(this)" value="3" class="filled-in"  />
                                <span>Viudo/a</span>
                            </label>
                        </p>
                        @if ($errors->has('estado_civil'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('estado_civil') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">location_on</i>
                        <input type="text" name="direccion" value="{{ old('direccion') }}" class="validate" required>
                        <label>Direccion</label>
                        @if ($errors->has('direccion'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('direccion') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">local_hospital</i>
                        <input type="text" name="isss" value="{{ old('isss') }}" class="validate">
                        <label>ISSS</label>
                        @if ($errors->has('isss'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('isss') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">cake</i>
                        <input  type="text" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}" class="datepicker" required>
                        <label>Fecha de nacimiento</label>
                        @if ($errors->has('fecha_nacimiento'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">call</i>
                        <input type="tel" name="telefono" value="{{ old('telefono') }}" required>
                        <label>telefono</label>
                        @if ($errors->has('telefono'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">calendar_today</i>
                        <input  type="text" name="ingreso" value="{{ old('ingreso') }}" class="validate" required>
                        <label>Año de ingreso</label>
                        @if ($errors->has('ingreso'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('ingreso') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_cargo" >
                            <option selected default value="">Sin Cargo </option>
                            @foreach ($cargos as $cargo)
                                <option value="{{ $cargo->id }}">{{ $cargo->nombre_cargo }}</option>
                            @endforeach
                        </select>
                        <label>Cargo</label>
                        @if ($errors->has('id_cargo'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('id_cargo') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_unidad" >
                            <option selected default value="">Sin unidad </option>
                        @foreach ($unidades as $unidad)
                                <option value="{{ $unidad->id }}">{{ $unidad->nombre_unidad }}</option>
                            @endforeach
                        </select>
                        <label>Unidad</label>
                        @if ($errors->has('id_unidad'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('id_unidad') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
    <br>
    <script>
        function check1(box)
        {
            if (box.checked) {
                document.getElementById("casado").checked=false;
                document.getElementById("viudo").checked=false;
            }
        }
        function check2(box) {
            if (box.checked) {
                document.getElementById("soltero").checked=false;
                document.getElementById("viudo").checked=false;

            }

        }
        function check3(box) {
            if (box.checked) {
                document.getElementById("soltero").checked = false;
                document.getElementById("casado").checked = false;
            }
        }
    </script>
@stop
