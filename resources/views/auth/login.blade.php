@extends('layouts.templatehome')
@section('content')
    <br>

<div class="row">
    <div class="col s6 push-s3 " >
            <form method="POST" action="{{ route('login') }}" class="card-panel">
                @csrf
                <h5 class="center-align col s12 blue-text"><b>Iniciar sesion</b></h5>

                <div class="input-field col s12">
                    <i class="material-icons prefix">email</i>
                    <input id="email" type="email" class="validate {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Ingresar E-mail') }}</label>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback red-text" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">lock</i>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <!--inputs del form-->
                <div class="col s12">
                    <p>
                        <label>
                            <input class="filled-in" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span>Recuerdame</span>
                        </label>
                    </p>

                </div>

                <div class="row">
                    <div class="col-md-8 offset-md-4">
                        <button class="btn col s12 btn-large blue darken-2" type="submit"><b>Iniciar</b></button>
                    @if (Route::has('password.request'))
                        <!--  <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}-->
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
</div>
@endsection
