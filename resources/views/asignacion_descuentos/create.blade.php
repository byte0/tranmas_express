@extends('layouts.mantenimientos')
@section('title') unidades | Create @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>AGREGANDO NUEVA ASIGNACION</h3>
            <form action="{{ url('/asignacion-descuentos/') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_empleado" >
                            @foreach ($empleados as $empleado)
                                <option  value="{{ $empleado->id }}">{{ $empleado->nombres . " " . $empleado->apellidos }}</option>
                            @endforeach
                        </select>
                        <label>Lista de Empleados</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_descuento" >
                            @foreach ($descuentos as $descuento)
                                <option  value="{{ $descuento->id }}">{{ $descuento->nombre_descuento }}</option>
                            @endforeach
                        </select>
                        <label>Lista de Descuentos</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">attach_money</i>
                        <input type="text" name="valor" class="validate" required>
                        <label>Valor </label>
                    </div>
                    <b class="col s12 l6 ">Forma del Descuento</b>
                    <br>
                    <p class="col s12 l3">
                        <label >
                            <input type="checkbox" id="porcentaje" onclick="check1(this)" name="forma_descuento"  value="1" class="filled-in" />
                            <span>% Porcentaje</span>
                        </label>
                    </p>
                    <p class="col s12 l3">
                        <label >
                            <input type="checkbox" id="dolares" onclick="check2(this)" name="forma_descuento"  value="2" class="filled-in"  />
                            <span>$ Dolares</span>
                        </label>
                    </p>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">today</i>
                        <input  type="text" name="fecha_asignacion" class="datepicker" required>
                        <label>Fecha de Asignacion</label>
                    </div>
                </div>
                <br>
                <hr>
                <div class="brown lighten-4 card-panel">
                    <h6 class="center-align"><b>Asignacion por periodo</b></h6>
                    <p>
                        Los siguientes campos solo son requeridos si el descuento que se esta agregando
                        tendrá una fecha de inicio y una fecha de finalización, si no es asi, puede omitirlos
                        sin ningún problema
                    </p>
                </div>
                <br>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="mes_inicio" >
                            @foreach ($meses as $mes)
                                <option  value="{{ $mes->id }}">{{ $mes->mes }}</option>
                            @endforeach
                        </select>
                        <label>Mes de Inicio</label>
                    </div>  <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="mes_final" >
                            @foreach ($meses as $mes)
                                <option  value="{{ $mes->id }}">{{ $mes->mes }}</option>
                            @endforeach
                        </select>
                        <label>Mes de Finalizacion</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">attach_money</i>
                        <input type="text" name="cuota_mes" class="validate">
                        <label>Cuota por mes</label>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
    <script>
        function check1(box)
        {
            if (box.checked) {
                document.getElementById("dolares").checked=false;
            }
        }
        function check2(box) {
            if (box.checked) {
                document.getElementById("porcentaje").checked=false;
            }

        }
    </script>
@endsection
