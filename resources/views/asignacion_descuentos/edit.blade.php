@extends('layouts.mantenimientos')
@section('title') unidades | Edit @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>EDITANDO A :
                @foreach($empleados as $empleado)
                    @if($empleado->id == $asignacion_desc->id_empleados)
                        {{ $empleado->nombres . " " .$empleado->apellidos }}
                    @endif
                @endforeach
            </h3>
            <form action="{{ url('/asignacion-descuentos/' . $asignacion_desc->id) }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_empleado" >
                            @foreach ($empleados as $empleado)
                                @if($empleado->id == $asignacion_desc->id_empleados)
                                    <option selected value="{{ $empleado->id }}">{{ $empleado->nombres . " " . $empleado->apellidos }}</option>
                                @endif
                                    <option  value="{{ $empleado->id }}">{{ $empleado->nombres . " " . $empleado->apellidos }}</option>
                            @endforeach
                        </select>
                        <label>Lista de Empleados</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_descuento" >
                            @foreach ($descuentos as $descuento)
                                @if($descuento->id == $asignacion_desc->id_descuentos)
                                    <option selected value="{{ $descuento->id }}">{{ $descuento->nombre_descuento }}</option>
                                @endif
                                <option  value="{{ $descuento->id }}">{{ $descuento->nombre_descuento }}</option>
                            @endforeach
                        </select>
                        <label>Lista de descuentos</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">attach_money</i>
                        <input type="text" name="valor" value="{{ $asignacion_desc->valor }}" class="validate" required>
                        <label>Valor </label>
                    </div>
                    <b class="col s12 l6 ">Forma del Descuento</b>
                    <br>
                @if( $asignacion_desc->forma_descuento == 1)
                    <p class="col s12 l3">
                        <label >
                            <input type="checkbox" name="forma_descuento" checked  value="1" class="filled-in" />
                            <span>% Porcentaje</span>
                        </label>
                    </p>
                    <p class="col s12 l3">
                        <label >
                            <input type="checkbox" name="forma_descuento"  value="2" class="filled-in"  />
                            <span>$ Dolares</span>
                        </label>
                    </p>
                @else
                    <p class="col s12 l3">
                        <label >
                            <input type="checkbox" name="forma_descuento"   value="1" class="filled-in" />
                            <span>% Porcentaje</span>
                        </label>
                    </p>
                    <p class="col s12 l3">
                        <label >
                            <input type="checkbox" name="forma_descuento" checked value="2" class="filled-in"  />
                            <span>$ Dolares</span>
                        </label>
                    </p>
                @endif
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">today</i>
                        <input  type="text" name="fecha_asignacion"  value="{{ $asignacion_desc->fecha_asignacion }}" class="datepicker" required>
                        <label>Fecha de Asignacion</label>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Cambios</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
