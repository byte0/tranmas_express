@extends('layouts.mantenimientos')
@section('title')Descripcion de Descuentos @endsection
@section('content')
<br>
<div class="card-panel z-depth-2">
    <div class="row">
        <div class="row">
            <h3 class="dark-text col s10"><b>LISTADO DE DESCUENTOS</b></h3>
            <br><br>
            <a href="{{ url('/descuento/pdf/descuento') }}" class="btn btn-success tooltipped right"
               data-position="top" data-tooltip="Descargar lista de Descuentos en PDF">
                <i class="material-icons ">print</i>
            </a>
        </div>
        <div class="divider col s12"></div>
        <div class="col s12">
                <table class="responsive-table white centered">
                    <thead>
                        <tr>
                            <th>Descuento</tH>
                            <th>Fecha de Asignacion</tH>
                            <th>Tipo Descuento</tH>
                            <th>Activo</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    @foreach ($descuentos as $descuento)
                        <tr>
                            <td>{{ $descuento->nombre_descuento }}</td>
                            <td>{{ $descuento->fecha_asignacion }}</td>
                            <td>
                                @foreach($tipo_descuentos as $tipo_desc)
                                    @if($tipo_desc->id == $descuento->idtipodescuento)
                                        {{ $tipo_desc->tipodescuentos }}
                                    @endif
                                @endforeach
                            </td>
                            @if($descuento->activo==1)
                                <td>{{ 'Activo '}}</td>
                            @else
                                <td class="red-text">{{ 'In Activo '}}</td>
                            @endif
                            <td>
                                <form class="" action="{{ url('/descuento/'. $descuento->id) }}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar el descuento ' + '{{ $descuento->nombre_descuento }}' )" type="submit" name="action"
                                            data-position="top" data-tooltip="Eliminar">
                                        <i class="material-icons">delete</i>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <a href="{{ url('/descuento/'. $descuento->id .'/edit') }}" class="btn btn-success blue tooltipped center" data-position="top" data-tooltip="Editar">
                                    <i class="material-icons">edit</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    
                    <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
                        <a class="btn-floating btn-large red">
                            <i class="large material-icons">apps</i>
                        </a>
                        <ul>
                            <li><a class="btn-floating red" href="{{ url('/descuento/create') }}"><i class="material-icons">add</i></a></li>
                        </ul>
                    </div>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
