@extends('layouts.mantenimientos')
@section('title') Descuentos @endsection
@section('content')
<form method="POST" action="{{ url('/descuento') }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="contatiner">
                <div class="card-panel">
                    <div class="row">
                        <div class="col s12">
                            <h3>AGREGANDO DESCUENTOS</h3>
                            <div class="divider"></div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">event</i>
                                <input type="text" class="validate" id="nombre_descuento" value="{{ old('nombre_descuento') }}" name="nombre_descuento">
                                <label for="nombre_descuento">Descuento</label>
                                @if ($errors->has('nombre_descuento'))
                                    <span class="red-text" role="alert">
                                        <strong>{{ $errors->first('nombre_descuento') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">assignment</i>
                                <input type="text" class="datepicker" value="{{ old('fecha_asignacion') }}" name="fecha_asignacion">
                                <label for="fecha_asignacion">Fecha Asignacion</label>
                                @if ($errors->has('fecha_asignacion'))
                                    <span class="red-text" role="alert">
                                        <strong>{{ $errors->first('fecha_asignacion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">activate</i>
                                <label>
                                    <input type="checkbox" class="filled-in" checked="checked" name="activo" value="1"/>
                                    <span>Activado</span>
                                </label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">event</i>
                                <select id="idtipodescuento" name="idtipodescuento">
                                    @foreach ($tipo_descuentos as $tipo_descuento)
                                        <option value="{{$tipo_descuento['id']}}">{{$tipo_descuento['tipodescuentos']}}</option>
                                    @endforeach
                                </select>
                                <label for="idtipodescuento">Tipo Descuento</label>
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                        </div>
                    </div>
                </div>
            </div>
    </form>
    <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
        <a href="javascript:history.back()" class="btn-floating btn-large red">
            <i class="large material-icons">keyboard_backspace</i>
        </a>
    </div>
@stop
