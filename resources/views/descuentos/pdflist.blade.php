<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="lavender">
    <h1 class="center"><b>TRANMAS EXPRES</b></h1>
</div>
<table class="responsive-table white centered highlight">
    <thead>
    <tr>
        <th>Descuento</tH>
        <th>Fecha de Asignacion</tH>
        <th>Tipo Descuento</tH>
        <th>Activo</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($descuentos as $descuento)
        <tr>
            <td>{{ $descuento->nombre_descuento }}</td>
            <td>{{ $descuento->fecha_asignacion }}</td>
            <td>
                @foreach($tipo_descuentos as $tipo_desc)
                    @if($tipo_desc->id == $descuento->idtipodescuento)
                        {{ $tipo_desc->tipodescuentos }}
                    @endif
                @endforeach
            </td>
            @if($descuento->activo==1)
                <td>{{ 'Activo '}}</td>
            @else
                <td class="red-text">{{ 'In Activo '}}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
<div class="lavender">
    <h5><b>contactanos</b></h5>
    <label><b>Direccion :</b>
        Lotificación las Victorias 6 Calle Pte Lt 2 – 4
        Sonsonate
    </label><br>
    <label><b>Telefono :</b>2484 5620</label><br>

</div>
<style>
    /*
Color fondo: #632432;
Color header: 246355;
Color borde: 0F362D;
Color iluminado: 369681;
*/
    body{
        background-color: #ffffff;
        font-family: Arial;
        margin: 0px;
        padding: 0px;
    }


    table{
        background-color: white;
        text-align: left;
        border-collapse: collapse;
        width: 100%;
    }

    th, td{
        padding: 1px;
        margin: 1px;
        text-align: center;
        width: 8%;
    }

    thead{
        background-color: darkblue;
        border-bottom: solid 5px cyan;
        color: white;
    }

    tr:nth-child(even){
        background-color: #ddd;
    }

    tr:hover td{
        background-color: #369681;
        color: white;
    }
    .center{
        text-align: center;
    }
    .right
    {
        text-align: center;
        color: blue;
    }
    .lavender
    {
        background-color: #E6E6FA;
        padding: 7px;
    }
</style>
</body>
</html>
