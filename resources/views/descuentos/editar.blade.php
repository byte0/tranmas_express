@extends('layouts.mantenimientos')
@section('title')| Editando Descuentos @endsection
@section('content')
    <div class="white">
        <div class="card-panel">
        <h3 class=""> Editando a {{ $descuento->nombre_descuento }}</h3>
        <div class="divider"></div>
        <br>
            <form action="{{ url('/descuento/'. $descuento->id)}}"  method="post">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">event</i>
                    <input type="text" name="nombre_descuento" id="nombre_descuento" class="validate" required value="{{ $descuento->nombre_descuento }}">
                    <label for="nombre_descuento">Descuento</label>
                        @if ($errors->has('nombre_descuento'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('nombre_descuento') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">event</i>
                    <input type="text" name="fecha_asignacion" class="datepicker" value="{{ $descuento->fecha_asignacion }}">
                        <label for="fecha_asignacion">Fecha Asignacion</label>
                        @if ($errors->has('fecha_asignacion'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('fecha_asignacion') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <p class="col s12 l4">
                        <label>
                            <input type="checkbox" name="activo"  checked="checked" value="1" class="filled-in"  />
                            <span>Activo</span>
                        </label>
                    </p>
                    <div class="input-field col s12 l4">
                        <i class="material-icons prefix">event</i>
                        <select name="tipo_descuento" id="tipo_descuento">
                            @foreach ($tipo_descuentos as $tipo_descuento)
                                <option value="{{$tipo_descuento['id']}}">{{$tipo_descuento['tipodescuentos']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Cambios</b></button>
                </div>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
