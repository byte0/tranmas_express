@extends('layouts.mantenimientos')
@section('title') Salida | Create @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>AGREGANDO NUEVA SALIDA DE REPUESTOS</h3>
            <form action="{{ url('/salida-repuestos/') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_repuesto">
                            @foreach ($repuestos as $repuesto)
                                <option value="{{ $repuesto->id }}"> {{  $repuesto->nombre_producto  }} </option>
                            @endforeach
                        </select>
                        <label>Repuesto</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">account_circle</i>
                        <input  type="text" disabled="true"   value="{{ Auth::user()->name }}" class="" >
                        <input  type="hidden" name="id_usuario_entreda"  value="{{ Auth::user()->id }}" class="" >
                        <label>Entregado por</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">account_circle</i>
                        <input type="text" name="id_usuario_recibe" class="validate" required>
                        <label>Recibido por:</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">add_circle</i>
                        <input type="number" name="cantidad_entregada" class="validate">
                        <label>Cantidad entregada</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">date_range</i>
                        <input  type="text" name="fecha_de_salida" class="datepicker" required>
                        <label>Fecha de salida</label>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
