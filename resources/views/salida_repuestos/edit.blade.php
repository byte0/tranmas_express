@extends('layouts.mantenimientos')
@section('title') Salida | edit @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>EDITANDO A : {{ $repuestoname->nombre_producto }} </h3>
            <form action="{{ url('/salida-repuestos/' . $salida_repuesto->id ) }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="id_repuesto">
                            <option selected default value="{{ $repuestoname->id }}"> {{  $repuestoname->nombre_producto  }} </option>
                            @foreach ($repuestos as $repuesto)
                                <option value="{{ $repuesto->id }}"> {{  $repuesto->nombre_producto  }} </option>
                            @endforeach
                        </select>
                        <label>Repuesto</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">date_range</i>
                        <input  type="text" name="fecha_de_salida" value="{{ $salida_repuesto->fecha_salida }}" class="datepicker" required>
                        <label>Fecha de salida</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">account_circle</i>
                        <input type="text" name="id_usuario_recibe" value="{{ $salida_repuesto->idusuario_recibe }}" class="validate">
                        <label>Recibido por:</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">add_circle</i>
                        <input type="number" name="cantidad_entregada" value="{{ $salida_repuesto->cantidad_entregada }}" class="validate">
                        <label>Cantidad entregada</label>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
