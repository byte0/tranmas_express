@extends('layouts.mantenimientos')
@section('title') Salida | Create @endsection
@section('content')
    <br>
    <div class=" z-depth-2 card-panel red lighten-5">
        <div class="row  ">
            <h4 class="red-text col s12 l10">Algo a salido mal, la cantidad ingresada supera al stock en existencia.</h4>
            <br>
            <i class="material-icons large col s12 right l2 blue-text">sentiment_very_dissatisfied</i>
        </div>
        <h4 class="col s12 teal-text">Por favor intentalo con una cantidad mas baja.</h4>
        <br>
        <div class="row ">
            @if ($error == "update")
                <a href="javascript:history.back()" class="waves-effect waves-light btn-large col s12"><i class="material-icons left">keyboard_backspace</i><b>Regresar al formulario de Editar</b></a>
            @elseif ($error == "store")
                <a href="javascript:history.back()" class="waves-effect waves-light btn-large col s12"><i class="material-icons left">keyboard_backspace</i><b>Regresar al formulario Crear</b></a>
            @endif
        </div>
    </div>

@endsection
