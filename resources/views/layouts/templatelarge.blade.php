<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>@yield('title','Default')|Administracion</title>
    <link rel="stylesheet"  href="{{ asset('css/materialize.css') }}">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  </head>
  <body class="teal lighten-5">
    @include('layouts.nav')
    <div class="">
      @yield('content')
    </div>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

    <script src="{{ asset('js/inicializaciones.js') }}"></script>
  </body>
</html>
