<!-- Dropdown Descuntos -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="{{ route('descuento.create')}}">Nuevo</a></li>
    <li><a href="{{ route('descuento.index') }}">Listar</a></li>
    <li><a href="#!">Bucar</a></li>
</ul>
<!-- DropDown Motoristas -->
<ul id="dropdown2" class="dropdown-content">
<li><a href="{{ url('/motoristas/create') }}">Nuevo</a></li>
<li><a href="{{ url('/motoristas') }}">Listar</a></li>
</ul>
<!-- DropDown Sesion -->
<ul id="dropdown3" class="dropdown-content">
    <li>
        <a class="" href="{{ route('logout') }}"
           onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
</ul>
<ul id="dropdown4" class="dropdown-content">
    <li><a href="{{ url('/repuestos/create') }}">Nuevo</a></li>
    <li><a href="{{ url('/repuestos') }}">Listar</a></li>
</ul>
<nav class="indigo darken-4">
        <div class="nav-wrapper">
            <a href="{{ url('/home') }}" class="brand-logo"><img src="{{ asset('imgs/logo.png') }}" class="responsive-img" alt="" width="100" height="67" style="margin-top: 5%"></a>
            <ul class="right hide-on-med-and-down">
                @guest
                    <li class=" brand-logo  center"><b>TRANMAS EXPRESS</b></li>
                     <!--
                     <li class="">
                        <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    -->
                    @if (Route::has('register'))
                        <li>
                        <!--  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>-->
                        </li>
                    @endif
                @else
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Descuento<i class="material-icons right">arrow_drop_down</i></a></li>
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown2">Motoristas<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown4">Repuestos<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown3" value="{{ csrf_token() }}">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                </ul>
            @endguest
        </div>
</nav>
