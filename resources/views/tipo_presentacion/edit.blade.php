@extends('layouts.mantenimientos')
@section('title') Presentacion | Edit @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>EDITANDO A : {{$presentacion->presentacion }}</h3>
            <form action="{{ url('/presentacion/' . $presentacion->id) }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">fiber_new</i>
                        <input type="text" name="presentacion" value="{{$presentacion->presentacion }}" class="validate" required>
                        <label>presentacion</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">create</i>
                        <input type="text" name="unidad_medida" value="{{$presentacion->unidad_medida }}" class="validate">
                        <label>Unidad de medida</label>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Cambios</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
