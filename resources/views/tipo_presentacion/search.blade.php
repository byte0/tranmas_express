@extends('layouts.mantenimientos')
@section('title')| Productos @endsection
@section('content')<br>
<div class="card-panel z-depth-2">
    <div class="row">
        <h3><b>Resultado de : </b> {{ $bus }}</h3>
        <div class="divider"></div>
        <div class="col s12">
            @if( count($presentaciones) == 0)
                <div class="row ">
                    <h3 class="blue-text col s12 l4">Lo lamento ...</h3>
                    <br>
                    <i class="material-icons medium col s12 l4 blue-text">sentiment_very_dissatisfied</i>
                </div>
                <h5>no se han encontrado resultados para {{ $bus }} ... </h5>
                <h5> vuelve a intentarlo con otros datos</h5>
            @else
                <table class="responsive-table white centered highlight">
                    <thead>
                    <tr>
                        <th>Presentacion</th>
                        <th>Unidad de medida</th>
                        <th colspan="2" >Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($presentaciones as $presentacion)
                        <tr>
                            <td>{{ $presentacion->presentacion }}</td>
                            <td>{{ $presentacion->unidad_medida }}</td>
                            <td>
                                <form class="col s12 m12 l11  right" action="{{ url('/presentacion/'. $presentacion->id) }}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar ha : ' + '{{ $presentacion->presentacion }}' )" type="submit" name="action"
                                            data-position="top" data-tooltip="Eliminar">
                                        <i class="material-icons">delete</i>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <a href="{{ url('/presentacion/'. $presentacion->id .'/edit') }}" class="btn left btn-success blue  tooltipped col"
                                   data-position="top" data-tooltip="Editar">
                                    <i class="material-icons ">edit</i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
            <a href=" {{ '/presentacion' }}" class="btn-floating btn-large red">
                <i class="large material-icons">keyboard_backspace</i>
            </a>
        </div>
        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">apps</i>
            </a>
            <ul>
                <li><a class="btn-floating red" href="{{ url('/presentacion/create') }}"><i class="material-icons">add</i></a></li>
                <li><a class="btn-floating yellow darken-1 modal-trigger" href="#modal1"><i class="material-icons">search</i></a></li>
            </ul>
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Buscar tipo de presentacion</h4>
                    <br>
                    <p>puedes buscar tipos de presentacion por nombre de presentacion o unidad de medida</p>
                    <div class="col s12">
                        <form class="col s12 m12 l12 row" action="{{ url('/presentacion/search') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                            <div class="input-field col s12 l12">
                                <i class="material-icons prefix">search</i>
                                <input type="text" name="bus" required>
                                <label>Buscar</label>
                            </div>
                            <div class="input-field col s12">
                                <button class="col s10 push-s1 btn waves-effect waves-light yellow blue-text" type="submit" name="action">Buscar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
