@extends('layouts.mantenimientos')
@section('title') Proudctos | Create @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3> Editando  a : {{ $repuestos->nombre_producto }}</h3>
            <form action="{{ url('/repuestos/' . $repuestos->id )}}" method="post">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">fiber_new</i>
                        <input type="text" name="nombre_producto" value="{{ $repuestos->nombre_producto }}" class="validate" required>
                        <label>Nombre de Producto</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">create</i>
                        <input type="number" name="cantidad" value="{{ $repuestos->cantidad }}" class="validate">
                        <label>Cantidad</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="presentacion" >
                            <option selected default value="{{ $tipo_presentaciones_actual->id }}"> {{  $tipo_presentaciones_actual->unidad_medida  }} </option>
                            @foreach ($tipo_presentaciones as $presentacion)
                                <option value="{{ $presentacion->id }}"> {{  $presentacion->unidad_medida  }} </option>
                            @endforeach
                        </select>
                        <label>Presentacion</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">keyboard</i>
                        <input type="text"  name="codigo_producto" value="{{ $repuestos->codigo_producto }}" class="validate" required>
                        <label>Codigo </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s12 l12">
                                <i class="material-icons prefix">mode_edit</i>
                                <textarea name="descripcion"  id="icon_prefix2" class="materialize-textarea">{{ $repuestos->descripcion }}</textarea>
                                <label for="icon_prefix2">Descripcion</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">date_range</i>
                        <input  type="text" name="fecha_ingreso" value="{{ $repuestos->fecha_ingreso }}"  class="datepicker" required>
                        <label>Fecha de ingreso</label>
                    </div>

                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">date_range</i>
                        <input  type="text" name="fecha_de_compra" value="{{ $repuestos->fecha_de_compra }}"  class="datepicker" required>
                        <label>Fecha de de compra</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l12">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="numero_estante">
                            <option selected default value="{{ $estante_actual->id }}"> {{  $estante_actual->estante  }} </option>
                            @foreach ($estantes as $estante)
                                <option value="{{ $estante->estante }}"> {{  $estante->estante  }} </option>
                            @endforeach
                        </select>
                        <label>Numero de Estante</label>
                    </div>

                    <div class="input-field col s12 l6">
                        <input  type="hidden" name="ingresado_por" value="{{ $repuestos->id_usuario }}"  class="" >
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">monetization_on</i>
                        <input type="text" name="precio" value="{{ $repuestos->precio }}" class="validate">
                        <label>precio</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">keyboard</i>
                        <input type="number" name="numero_factura" value="{{ $repuestos->numero_factura }}" class="validate">
                        <label>Numero de factura</label>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Cambios</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
