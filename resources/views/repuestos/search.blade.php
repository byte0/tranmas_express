@extends('layouts.mantenimientos')
@section('title')| Productos @endsection
@section('content')<br>
<div class="card-panel z-depth-2">
    <div class="row">
        <h3>Resultado de : {{ $bus }}</h3>
        <div class="divider"></div>
        <div class="col s12">
            @if( count($repuestos) == 0)
                <div class="row ">
                    <h3 class="blue-text col s12 l4">Lo lamento ...</h3>
                    <br>
                    <i class="material-icons medium col s12 l4 blue-text">sentiment_very_dissatisfied</i>
                </div>
                <h5>no se han encontrado resultados para {{ $bus }} ... </h5>
                <h5> vuelve a intentarlo con otros datos</h5>
            @else
                <table class="table centered">
                    <tr>
                        <th>Nombre Producto</th>
                        <th>Cantidad</th>
                        <th>Presentacion</th>
                        <th>Codigo</th>
                        <th>Descripcion</th>
                        <th>Comprado el</th>
                        <th>Ingresado el</th>
                        <th>Ingresado por</th>
                        <th>Nº Estante</th>
                        <th>Nº Factura</th>
                        <th>Precio</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach ($repuestos as $repuesto)
                        <tr>
                            <td>{{ $repuesto->nombre_producto }}</td>
                            <td>{{ $repuesto->cantidad }}</td>
                            <td> @foreach($tipo_presentaciones as $presentacion)
                                    @if($presentacion->id == $repuesto->id_tipo_presentacion)
                                        {{ $presentacion->unidad_medida }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ $repuesto->codigo_producto }}</td>
                            <td>{{ $repuesto->descripcion }}</td>
                            <td>{{ $repuesto->fecha_de_compra }}</td>
                            <td>{{ $repuesto->fecha_ingreso }}</td>
                            <td> @foreach($users as $user)
                                    @if($user->id == $repuesto->id_usuario)
                                        {{ $user->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td> @foreach($estantes as $estantee)
                                    @if($estantee->id == $repuesto->id_estante)
                                        {{ $estantee->estante }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ $repuesto->numero_factura }}</td>
                            <td>{{ $repuesto->precio }}</td>

                            <td>
                                <form class="col s12 m12 l12" action="{{ url('/repuestos/'. $repuesto->id) }}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar ha : ' + '{{ $repuesto->nombre_producto }}' )" type="submit" name="action"
                                            data-position="top" data-tooltip="Eliminar">
                                        <i class="material-icons">delete</i>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <div class="show-on-small hide-on-large-only"> <br></div>
                                <a href="{{ url('/repuestos/'. $repuesto->id .'/edit') }}" class="btn btn-success blue tooltipped center"
                                   data-position="top" data-tooltip="Editar">
                                    <i class="material-icons ">edit</i></a>
                            </td>
                            <td>
                                <div class="show-on-small hide-on-large-only"> <br></div>
                                <a href="{{ url('/repuestos/pdf/'. $repuesto->id ) }}" class="btn btn-success tooltipped center"
                                   data-position="top" data-tooltip="Descargar PDF">
                                    <i class="material-icons ">print</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            @endif
        </div>
        <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
            <a href=" {{ '/repuestos' }}" class="btn-floating btn-large red">
                <i class="large material-icons">keyboard_backspace</i>
            </a>
        </div>
        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">apps</i>
            </a>
            <ul>
                <li><a class="btn-floating red" href="{{ url('/productos/create') }}"><i class="material-icons">add</i></a></li>
                <li><a class="btn-floating yellow darken-1 modal-trigger" href="#modal1"><i class="material-icons">search</i></a></li>
            </ul>
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Buscar Motorista</h4>
                    <br>
                    <p>puedes buscar repuestos por nombre & codigo o fecha & precio</p>
                    <div class="col s12">
                        <form class="col s12 m12 l12 row" action="{{ url('/productos/search') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                            <div class="input-field col s12 l12">
                                <i class="material-icons prefix">search</i>
                                <input type="text" name="bus" required>
                                <label>Buscar</label>
                            </div>
                            <div class="input-field col s12">
                                <button class="col s10 push-s1 btn waves-effect waves-light yellow blue-text" type="submit" name="action">Buscar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
