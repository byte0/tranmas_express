<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
<div class="lavender">
    <h1 class="center">Tranma Express</h1>
    <h4><b>Repuestos : </b></h4><br>
    <b>Datos: </b><br>
</div>
<div>
    <hr>
    <h5 class="right">Repuesto</h5>
    <label><b> Nombre :</b> {{ $repuestos->nombre_producto }}</label><br>
    <label><b>Cantidad :</b> {{ $repuestos->cantidad }} </label><br>
    <label><b>Presentacion :</b>
        @foreach($tipo_presentaciones as $presentacion)
            @if($presentacion->id == $repuestos->id_tipo_presentacion)
                {{ $presentacion->unidad_medida }}
            @endif
        @endforeach
    </label><br>
    <label><b>Descripcion :</b> {{ $repuestos->descripcion }} </label><br>
    <label><b>Codigo :</b> {{ $repuestos->codigo_producto }} </label><br>
    <label><b>Precio :</b> {{ $repuestos->precio }} </label><br>
    <hr>
</div>
<div>
    <h5 class="right">Detalle</h5>
    <label>
        <b> Ingresado Por :</b>
        @foreach($users as $user)
            @if($user->id == $repuestos->id_usuario)
                {{ $user->name }}
            @endif
        @endforeach
    </label>
    <br>
    <label>
        <b>Numero de Estante :</b>
        @foreach($estantes as $estantee)
            @if($estantee->id == $repuestos->id_estante)
                {{ $estantee->estante }}
            @endif
        @endforeach
    </label>
    <br>
    <label><b>Numero de Factura :</b> {{ $repuestos->numero_factura }} </label><br>
    <label><b>Fecha de Compra :</b> {{ $repuestos->fecha_de_compra }} </label><br>
    <label><b>Fecah de Ingreso :</b> {{ $repuestos->fecha_ingreso }} </label><br>

    <hr>
</div>
<div class="lavender">
    <h5><b>contactanos</b></h5>
    <label><b>Direccion :</b>
        Lotificación las Victorias 6 Calle Pte Lt 2 – 4
        Sonsonate
    </label><br>
    <label><b>Telefono :</b>2484 5620</label><br>

</div>
<style>
    /*
Color fondo: #632432;
Color header: 246355;
Color borde: 0F362D;
Color iluminado: 369681;
*/
    body{
        background-color: #ffffff;
        font-family: Arial;
        margin: 0px;
        padding: 0px;
    }


    table{
        background-color: white;
        text-align: left;
        border-collapse: collapse;
        width: 100%;
    }

    th, td{
        padding: 1px;
        margin: 1px;
        text-align: center;
        width: 8%;
    }

    thead{
        background-color: darkblue;
        border-bottom: solid 5px cyan;
        color: white;
    }

    tr:nth-child(even){
        background-color: #ddd;
    }

    tr:hover td{
        background-color: #369681;
        color: white;
    }
    .center{
        text-align: center;
    }
    .right
    {
        text-align: center;
        color: blue;
    }
    .lavender
    {
        background-color: #E6E6FA;
        padding: 7px;
    }
</style>
</body>
</html>

