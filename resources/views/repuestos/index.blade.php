@extends('layouts.mantenimientos')
@section('title')| Motoristas @endsection
@section('content')
    <br>
    <div class="card-panel">
        <div class="row">
            <a href="{{ url('/salida-repuestos') }}" class="col s12 l6 red white-text waves-effect waves-light"><h5><i class="small left material-icons">folder_open</i><b>Ver salida de Repuestos</b></h5></a>
            <a href="{{ url('/presentacion') }}" class="col s12 l6 teal white-text waves-effect waves-light"><h5><i class="small left material-icons">folder_open</i><b>Ver tipo de presentaciones</b></h5></a>
        </div>
        <h3 class="dark-text"> <b> Listado de Repuestos </b></h3>
        <div class="divider"></div>
        <table class="responsive-table white centered highlight">
            <thead>
            <tr>
                <th>Nombre Producto</th>
                <th>Cantidad</th>
                <th>Presentacion</th>
                <th>Codigo</th>
                <th>Descripcion</th>
                <th>Comprado el</th>
                <th>Ingresado el</th>
                <th>Ingresado por</th>
                <th>Nº Estante</th>
                <th>Nº Factura</th>
                <th>Precio</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($repuestos as $repuesto)
                <tr>
                    <td>{{ $repuesto->nombre_producto }}</td>
                    <td>{{ $repuesto->cantidad }}</td>
                    <td>
                        @foreach($tipo_presentaciones as $presentacion)
                            @if($presentacion->id == $repuesto->id_tipo_presentacion)
                                {{ $presentacion->unidad_medida }}
                            @endif
                        @endforeach
                    </td>
                    <td>{{ $repuesto->codigo_producto }}</td>
                    <td>{{ $repuesto->descripcion }}</td>
                    <td>{{ $repuesto->fecha_de_compra }}</td>
                    <td>{{ $repuesto->fecha_ingreso }}</td>
                    <td>
                        @foreach($users as $user)
                            @if($user->id == $repuesto->id_usuario)
                                {{ $user->name }}
                            @endif
                        @endforeach
                    </td>
                    <td>
                        @foreach($estantes as $estantee)
                            @if($estantee->id == $repuesto->id_estante)
                                {{ $estantee->estante }}
                            @endif
                        @endforeach
                    </td>
                    <td>{{ $repuesto->numero_factura }}</td>
                    <td>{{ $repuesto->precio }}</td>

                    <td>
                        <form class="col s12 m12 l12" action="{{ url('/repuestos/'. $repuesto->id) }}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar ha : ' + '{{ $repuesto->nombre_producto }}' )" type="submit" name="action"
                                    data-position="top" data-tooltip="Eliminar">
                                <i class="material-icons">delete</i>
                            </button>
                        </form>
                    </td>
                    <td>
                        <div class="show-on-small hide-on-large-only"> <br></div>
                        <a href="{{ url('/repuestos/'. $repuesto->id .'/edit') }}" class="btn btn-success blue tooltipped center"
                           data-position="top" data-tooltip="Editar">
                            <i class="material-icons ">edit</i>
                        </a>
                    </td>
                    <td>
                        <div class="show-on-small hide-on-large-only"> <br></div>
                        <a href="{{ url('/repuestos/pdf/'. $repuesto->id ) }}" class="btn btn-success tooltipped center"
                           data-position="top" data-tooltip="Descargar PDF">
                            <i class="material-icons ">print</i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">apps</i>
            </a>
            <ul>
                <li><a class="btn-floating red" href="{{ url('/repuestos/create') }}"><i class="material-icons">add</i></a></li>
                <li><a class="btn-floating yellow darken-1 modal-trigger" href="#modal1"><i class="material-icons">search</i></a></li>
            </ul>
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Buscar Motorista</h4>
                    <br>
                    <p>puedes buscar Repuestos por nombre de producto, codigo & numero de factura</p>
                    <div class="col s12">
                        <form class="col s12 m12 l12 row" action="{{ url('/repuestos/search') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                            <div class="input-field col s12 l12">
                                <i class="material-icons prefix">search</i>
                                <input type="text" name="bus" required>
                                <label>Buscar</label>
                            </div>
                            <div class="input-field col s12">
                                <button class="col s10 push-s1 btn waves-effect waves-light yellow blue-text" type="submit" name="action">Buscar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection
