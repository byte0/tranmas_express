@extends('layouts.mantenimientos')
@section('title') unidades | Create @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>AGREGANDO NUEVA UNIDAD</h3>
            <form action="{{ url('/unidades/') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">fiber_new</i>
                        <input type="text" name="nombre_unidad" class="validate" required>
                        <label>Nombre</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">create</i>
                        <input type="text" name="numero_placa" class="validate">
                        <label>Numero de placa</label>
                    </div>
                </div>
                <div class="row">
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="textarea1" name="descripcion" class="materialize-textarea"></textarea>
                                <label for="textarea1">Descripcion</label>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">folder_open</i>
                        <select name="estado">
                            <option value="0">Lista para funcionar</option>
                            <option value="1">Necesita reparaciones</option>
                            <option value="2">Deshabilitada</option>
                        </select>
                        <label>Presentacion</label>
                    </div>                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
