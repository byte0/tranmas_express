@extends('layouts.mantenimientos')
@section('title')| Prsentacion @endsection
@section('content')
    <br>
<div class="card-panel">
        <h3><b>Resultado de : </b> {{ $bus }}</h3>
        <div class="divider"></div>
        <div class="col s12">
            @if( count($unidades) == 0)
                <div class="row ">
                    <h3 class="blue-text col s12 l4">Lo lamento ...</h3>
                    <br>
                    <i class="material-icons medium col s12 l4 blue-text">sentiment_very_dissatisfied</i>
                </div>
                <h5>no se han encontrado resultados para {{ $bus }} ... </h5>
                <h5> vuelve a intentarlo con otros datos</h5>
            @else
                <table class="responsive-table white centered highlight">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Placa</th>
                        <th>Descripcion</th>
                        <th>Estado</th>
                        <th colspan="2">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($unidades as $unidad)
                        <tr>
                            <td>{{ $unidad->nombre_unidad }}</td>
                            <td>{{ $unidad->numero_placa }}</td>
                            <td>{{ $unidad->descripcion }}</td>
                            <td>{{ $unidad->estado }}</td>

                            <td>
                                <form class="col s12 m12 l12 right" action="{{ url('/unidades/'. $unidad->id) }}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar ha : ' + '{{ $unidad->unidades }}' )" type="submit" name="action"
                                            data-position="top" data-tooltip="Eliminar">
                                        <i class="material-icons">delete</i>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <div class="show-on-small hide-on-large-only"> <br></div>
                                <a href="{{ url('/unidades/'. $unidad->id .'/edit') }}" class="left btn btn-success blue tooltipped center"
                                   data-position="top" data-tooltip="Editar">
                                    <i class="material-icons ">edit</i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif

        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">apps</i>
            </a>
            <ul>
                <li><a class="btn-floating red" href="{{ url('/unidades/create') }}"><i class="material-icons">add</i></a></li>
                <li><a class="btn-floating yellow darken-1 modal-trigger" href="#modal1"><i class="material-icons">search</i></a></li>
            </ul>
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Buscar tipo de unidades</h4>
                    <br>
                    <p>puedes buscar tipos de unidades por nombre de unidades o numero de placa</p>
                    <div class="col s12">
                        <form class="col s12 m12 l12 row" action="{{ url('/unidades/search') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                            <div class="input-field col s12 l12">
                                <i class="material-icons prefix">search</i>
                                <input type="text" name="bus" required>
                                <label>Buscar</label>
                            </div>
                            <div class="input-field col s12">
                                <button class="col s10 push-s1 btn waves-effect waves-light yellow blue-text" type="submit" name="action">Buscar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                </div>
            </div>
        </div>

        <br>
    </div>
    <!--boton fotante echo con css normal, debido a que con materializecss no permite tener dos en la misma page-->
    <div class="" >
        <a href="{{ '/repuestos' }}" class="btn-floating btn-large red" style=" position: fixed;  float: left; bottom: 45px; left: 24px;">
            <i class="large material-icons">keyboard_backspace</i>
        </a>
    </div>
@endsection
