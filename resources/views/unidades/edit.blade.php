@extends('layouts.mantenimientos')
@section('title') unidades | Create @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>EDITANDO A : {{ $unidad->nombre_unidad }}</h3>
            <form action="{{ url('/unidades/' . $unidad->id) }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">fiber_new</i>
                        <input type="text" name="nombre_unidad" value="{{ $unidad->nombre_unidad }}" class="validate" required>
                        <label>Nombre</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">create</i>
                        <input type="text" name="numero_placa" value="{{ $unidad->numero_placa }}" class="validate">
                        <label>Numero de placa</label>
                    </div>
                </div>
                <div class="row">
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="textarea1" name="descripcion" class="materialize-textarea">{{ $unidad->descripcion }}</textarea>
                            <label for="textarea1">Descripcion</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <b class="col s12 center-align">Estado Civil</b>
                    <br>
                    @if ($unidad->estado == 0)
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado" value="0" checked="checked" class="filled-in" />
                                <span>Lista para funcionar</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado"   value="1" class="filled-in"  />
                                <span>Necesita reparaciones</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado" value="2" class="filled-in"  />
                                <span>Deshabilitada</span>
                            </label>
                        </p>
                    @elseif ($unidad->estado == 1 )                       <p class="col s12 l4">
                        <label >
                            <input type="checkbox" name="estado" value="0" class="filled-in" />
                            <span>Lista para funcionar</span>
                        </label>
                    </p>
                    <p class="col s12 l4">
                        <label >
                            <input type="checkbox" name="estado" checked="checked" value="1" class="filled-in"  />
                            <span>Necesita reparaciones</span>
                        </label>
                    </p>
                    <p class="col s12 l4">
                        <label >
                            <input type="checkbox" name="estado" value="2" class="filled-in"  />
                            <span>Deshabilitada</span>
                        </label>
                    </p>
                    @else
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado" value="0" class="filled-in" />
                                <span>Lista para funcionar</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado"  value="1" class="filled-in"  />
                                <span>Necesita reparaciones</span>
                            </label>
                        </p>
                        <p class="col s12 l4">
                            <label >
                                <input type="checkbox" name="estado"  checked="checked" value="2" class="filled-in"  />
                                <span>Deshabilitada</span>
                            </label>
                        </p>
                    @endif

                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
