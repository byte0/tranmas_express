<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <div class="lavender">
        <h1 class="center">Tranma Express</h1>
        <label><b>Unidad de Transporte </b></label><br>
        <b>Datos: </b><br>
    </div>
    <div>
        <hr>
        <h5 class="right">detalles</h5>
        <label><b> Nombre :</b> {{$unidad->nombre_unidad }}</label><br>
        <label><b>Numero de placa :</b>{{ $unidad->numero_placa }}</label><br>
        <label><b>Estado Civil :</b>
            @if ($unidad->estado == 0)
                Lista para funcionar
            @elseif ($unidad->estado == 1 )
                Necesita reparaciones
            @else
                Deshabilitada
            @endif
        </label><br>
        <label><b>Descripcion :</b>{{ $unidad->descripcion }}</label><br>
        <hr>
    </div>
    <div class="lavender">

        <h5><b>contactanos</b></h5>
        <label><b>Direccion :</b>
            Lotificación las Victorias 6 Calle Pte Lt 2 – 4
            Sonsonate
        </label><br>
        <label><b>Telefono :</b>2484 5620</label><br>

    </div>

<style>
    /*
Color fondo: #632432;
Color header: 246355;
Color borde: 0F362D;
Color iluminado: 369681;
*/
    body{
        background-color: #ffffff;
        font-family: Arial;
        margin: 0px;
        padding: 0px;
    }


    table{
        background-color: white;
        text-align: left;
        border-collapse: collapse;
        width: 100%;
    }

    th, td{
        padding: 1px;
        margin: 1px;
        text-align: center;
        width: 8%;
    }

    thead{
        background-color: darkblue;
        border-bottom: solid 5px cyan;
        color: white;
    }

    tr:nth-child(even){
        background-color: #ddd;
    }

    tr:hover td{
        background-color: #369681;
        color: white;
    }
    .center{
        text-align: center;
    }
    .right
    {
        text-align: center;
        color: blue;
    }
    .lavender
    {
        background-color: #E6E6FA;
        padding: 7px;
    }
</style>
</body>
</html>

