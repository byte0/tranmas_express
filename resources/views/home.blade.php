@extends('layouts.templatehome')

@section('content')
    <br><br>
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<div class="row">
    <a href="{{ url('/empleados') }}" class="">
        <div class="col s12 l3 ">
            <div class="card">
                <div class="card-image  blue lighten-4 z-depth-3">
                    <img src="{{ asset('imgs/home/empleados.png') }}">
                    <span class="card-title red-text"><b></b></span>
                </div>
                <div class="card-action center-align">
                    <b>EMPLEADOS</b>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ url('/unidades') }}">
        <div class="col s12 l3">
            <div class="card">
                <div class="card-image yellow lighten-4 z-depth-3">
                    <img src="{{ asset('imgs/home/buses.png') }}">
                    <span class="card-title red-text"><b></b></span>
                </div>
                <div class="card-action center-align">
                    <b>UNIDADES</b>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ url('/repuestos') }}">
        <div class="col s12 l3">
            <div class="card">
                <div class="card-image red lighten-4 z-depth-3">
                    <img src="{{ asset('imgs/home/repuestos.png') }}">
                    <span class="card-title red-text"><b></b></span>
                </div>
                <div class="card-action center-align">
                    <b>REPUESTOS</b>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ url('/planillas') }}">
        <div class="col s12 l3">
            <div class="card">
                <div class="card-image green lighten-4 z-depth-3">
                    <img src="{{ asset('imgs/home/planilla.png') }}">
                    <span class="card-title red-text"><b></b></span>
                </div>
                <div class="card-action center-align">
                    <b>PLANILLAS</b>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="row">
    <a href="{{ url('/descuento') }}">
        <div class="col s12 l3">
            <div class="card">
                <div class="card-image orange lighten-4 z-depth-3">
                    <img src="{{ asset('imgs/home/descuentos.png') }}">
                    <span class="card-title red-text"><b></b></span>
                </div>
                <div class="card-action center-align">
                    <b>DESCUENTOS</b>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ url('/cargos') }}">
        <div class="col s12 l3">
            <div class="card">
                <div class="card-image  deep-orange lighten-2 z-depth-3">
                    <img src="{{ asset('imgs/home/cargos.png') }}">
                    <span class="card-title red-text"><b></b></span>
                </div>
                <div class="card-action center-align">
                    <b>CARGOS</b>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ url('/asignacion-descuentos') }}">
        <div class="col s12 l3">
            <div class="card">
                <div class="card-image light-blue lighten-3 z-depth-3">
                    <img src="{{ asset('imgs/home/asignacion_descuentos.png') }}">
                    <span class="card-title red-text"><b></b></span>
                </div>
                <div class="card-action center-align">
                    <b>Asignar Descuentos</b>
                </div>
            </div>
        </div>
    </a>
</div>

@endsection
