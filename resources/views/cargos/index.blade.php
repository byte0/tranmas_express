@extends('layouts.mantenimientos')
@section('title')| Cargos @endsection
@section('content')
    <br>
    <div class="card-panel">
        <div class="row">
            <h3 class="dark-text col s10"><b>LISTADO DE CARGOS</b></h3>
            <br><br>
            <div class="show-on-small hide-on-large-only"> <br></div>
            <a href="{{ url('/cargos/pdf/cargos') }}" class="btn btn-success tooltipped right"
               data-position="top" data-tooltip="Descargar lista de cargos en PDF">
                <i class="material-icons ">print</i>
            </a>
        </div>
        <div class="divider"></div>
        <table class="responsive-table white centered highlight">
            <thead>
            <tr>
                <th>Cargo</th>
                <th>Descripcion</th>
                <th>Sueldo</th>
                <th colspan="2">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cargos as $cargo)
                <tr>
                    <td>{{ $cargo->nombre_cargo }}</td>
                    <td>{{ $cargo->descripcion }}</td>
                    <td>$ {{ $cargo->sueldo_bruto }}</td>
                    <td>
                        <form class="col s12 m12 l12 center" action="{{ url('/cargos/'. $cargo->id) }}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn waves-effect waves-light red center tooltipped" onclick="return confirm('esta seguro que desea eliminar ha : ' + '{{ $cargo->nombre_cargo }}' )" type="submit" name="action"
                                    data-position="top" data-tooltip="Eliminar">
                                <i class="material-icons">delete</i>
                            </button>
                        </form>
                    </td>
                    <td>
                        <div class="show-on-small hide-on-large-only"> <br></div>
                        <a href="{{ url('/cargos/'. $cargo->id .'/edit') }}" class="center btn btn-success blue tooltipped center"
                           data-position="top" data-tooltip="Editar">
                            <i class="material-icons ">edit</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">apps</i>
            </a>
            <ul>
                <li><a class="btn-floating red" href="{{ url('/cargos/create') }}"><i class="material-icons">add</i></a></li>
                <li><a class="btn-floating yellow darken-1 modal-trigger" href="#modal1"><i class="material-icons">search</i></a></li>
            </ul>
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Buscar tipo de cargos</h4>
                    <br>
                    <p>puedes buscar tipos de cargos por nombre de cargos o numero de placa</p>
                    <div class="col s12">
                        <form class="col s12 m12 l12 row" action="{{ url('/cargos/search') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                            <div class="input-field col s12 l12">
                                <i class="material-icons prefix">search</i>
                                <input type="text" name="bus" required>
                                <label>Buscar</label>
                            </div>
                            <div class="input-field col s12">
                                <button class="col s10 push-s1 btn waves-effect waves-light yellow blue-text" type="submit" name="action">Buscar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                </div>
            </div>
        </div>

        <br>
    </div>
    <!--boton fotante echo con css normal, debido a que con materializecss no permite tener dos en la misma page-->
    <div class="" >
        <a href="{{ '/repuestos' }}" class="btn-floating btn-large red" style=" position: fixed;  float: left; bottom: 45px; left: 24px;">
            <i class="large material-icons">keyboard_backspace</i>
        </a>
    </div>
@endsection
