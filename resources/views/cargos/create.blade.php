@extends('layouts.mantenimientos')
@section('title') unidades | Create @endsection
@section('content')
    <br>
    <div class="card-panel z-depth-2">
        <div class="row">
            <h3>AGREGANDO NUEVA UNIDAD</h3>
            <form action="{{ url('/cargos/') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix">fiber_new</i>
                        <input type="text" name="nombre_cargo" value="{{ old('nombre_cargo') }}" class="validate" required>
                        <label>Nombre | cargo</label>
                        @if ($errors->has('nombre_cargo'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('nombre_cargo') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12 l6">
                        <i class="material-icons prefix ">attach_money</i>
                        <input type="text" name="sueldo_bruto" value="{{ old('sueldo_bruto') }}" class="validate">
                        <label>sueldo bruto </label>
                        @if ($errors->has('sueldo_bruto'))
                            <span class="red-text" role="alert">
                                <strong>{{ $errors->first('sueldo_bruto') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="textarea1" name="descripcion" class="materialize-textarea">{{ old('descripcion') }}</textarea>
                            <label for="textarea1">Descripcion</label>
                            @if ($errors->has('descripcion'))
                                <span class="red-text" role="alert">
                                <strong>{{ $errors->first('descripcion') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <br>
                    <button class="btn col s12 l8 push-l2 btn-large" type="submit"><b>Guardar Nuevo</b></button>
                </div>
                <br>
            </form>
            <div class="fixed-action-btn horizontal" style="bottom: 45px; left: 24px;">
                <a href="javascript:history.back()" class="btn-floating btn-large red">
                    <i class="large material-icons">keyboard_backspace</i>
                </a>
            </div>
        </div>
    </div>
@endsection
